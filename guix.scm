;;; Kaka Farm web site
;;; Copyright © 2024 Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This file is part of Kaka Farm site.

(define-module (guix))

(import
 (ice-9 textual-ports)

 (gnu packages base)
 (gnu packages bash)
 (gnu packages commencement)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages less)
 (gnu packages maths)
 (gnu packages rsync)
 (gnu packages web)

 (guix build-system copy)
 (guix build-system gnu)
 (guix build-system trivial)
 (guix modules)
 (guix build utils)
 (guix gexp)
 (guix git-download)
 (prefix (guix licenses) license:)
 (guix packages)
 )

(define-public kaka-farm
  (package
    (name "kaka-farm")
    (version "0.0.0")
    (source (local-file (dirname (current-filename)) #:recursive? #t))
    (build-system copy-build-system)
    (native-inputs
     (list
      bash-minimal
      coreutils
      glibc-locales
      guile-3.0
      haunt
      units
      ))
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'build
            (lambda _
              (import (guix build utils))
              (setenv "LC_ALL" "en_IL.utf8")
              (setenv "TZ" "Israel")
              (invoke (string-append #$gnu-make "/bin/make") "site")
              (copy-recursively "build/pages-site/" "site")
              (copy-recursively "build/blog-site/" "site")
              #t)))
      #:install-plan
      #~(let ()
          '(("site" "var/www/kaka.farm")))
      ))
    (home-page "https://codeberg.org/kakafarm/kaka.farm/")
    (synopsis
     "Kaka Farm site")
    (description
     "Kaka Farm by Yuval Langer.")
    (license license:expat)
    ))

kaka-farm
