title: Scheme Macro Guides For A Myriad of Neurological and Emotional Afflictions
date: 2024-05-25 23:09
tags: lisp, macros, neurodiversity, nutcases, programming, scheme
---

Scheme macros seem to fascinate and attract unhinged people from
across the globe, resulting in a series of guides dedicated to the
craft of macros for a wide range of brain afflications.

Some examples follow and may be updated in the future.

## An Advanced Syntax-Rules Primer for the Mildly Insane by Al Petrofsky:

<https://web.archive.org/web/20040930194642/https://petrofsky.org/src/primer.txt>

## Various comp.lang.scheme discussions:

<https://groups.google.com/g/comp.lang.scheme/c/hsM4g33joCA/m/GbZ1F-HGbOsJ>

## mnieper's Extending a Language

The web page:

<https://mnieper.github.io/scheme-macros/README.html>

and the source repository:

<https://github.com/mnieper/scheme-macros>

## JRM's Syntax-rules Primer for the Merely Eccentric

A cleaned up version by the talented Zipheir:

<https://www.sigwinch.xyz/misc/jrms_syntax-rules_primer.txt>

Some other(?) versions:

<https://hipster.home.xs4all.nl/lib/scheme/gauche/define-syntax-primer.txt>

If you dislike beautifully rendered text files and prefer using
uncomfortable PDF files that can never quite fit on a screen or on an
e-ink portable device, and do not work well with text-to-speech
engines:

<http://www.phyast.pitt.edu/~micheles/syntax-rules.pdf>
