title: Wrote Guix Package Definitions For Srfi-235 (Combinators) And Srfi-253 (Type Checking).
date: 2024-09-07 16:07
tags: freesoftware, fsf, gnu, guix, lisp, scheme, srfi
---

They are both on [my Guix channel] as `guile-srfi-235` and `guile-srfi-253`.  They're meant for use in the [GNU Guile] Scheme implementation.

The definitions are in the [./kakafarm/packages/guile-xyz.scm] file.

[SRFI-235] was finalised in 2023, but [SRFI-253] is still in draft form, so it may still change drastically until its deadline, which is in 2024-10-12, about a month from now, please submit your gripes and suggestions as described in <https://srfi.schemers.org/srfi-253/>!

I also submitted the SRFI-235 package definition as a patch to the official Guix channel: <https://debbugs.gnu.org/cgi/bugreport.cgi?bug=73096>

I want to use SRFI-235 as a way to comfortably define types, as suggested in the SRFI-253 document, so I went through this packaging yak shaving.  Hopefully it'll be useful to others.

[my Guix channel]: https://codeberg.org/kakafarm/guix-kakafarm-channel/
[GNU Guile]: https://www.gnu.org/software/guile/
[./kakafarm/packages/guile-xyz.scm]: https://codeberg.org/kakafarm/guix-kakafarm-channel/src/branch/master/kakafarm/packages/guile-xyz.scm
[SRFI-235]: https://srfi.schemers.org/srfi-235/
[SRFI-253]: https://srfi.schemers.org/srfi-253/
