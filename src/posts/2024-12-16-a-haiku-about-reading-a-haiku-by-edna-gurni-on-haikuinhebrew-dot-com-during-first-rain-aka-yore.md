title: Haiku About Reading Haiku By Edna Gurni On Haikuinhebrew Dot Com During First Rain AKA Yore.
date: 2024-12-16 13:00
tags: archive, haiku, haikuinhebrew, hebrew, kaka, poetry, poo, poop
---

A few years ago, maybe on the day of 2020-11-06, I wrote a haiku about reading a haiku about poop while the first rains are falling.

https://haikuinhebrew.com/2020/11/06/5490/#comment-1358

The original by Edna Gorni(?) (keeping the signature (in parentheses)):

> צָפָה בְּמֵי הָאַסְלָה
>
> הַפְתָּעָה
>
> שֶׁמִּישֶׁהוּ הִשְׁאִיר
>
> (עדנה גורני)

My haiku, the first version:

> לבד בגשם ראשון
>
> היקיקומורי
>
> קורא הייקו על חרא

The second fixed version:

> בגשם ראשון
>
> היקיקומורי קורא
>
> הייקו על חרא

Thank you Margarita for saving the first version.

The thread, with the haiku, which I had copied and pasted here like a filthy rube:

> Yuval Langer
> 6 בנובמבר 2020 ב- 15:21
>
> לבד בגשם ראשון
>
> היקיקומורי
>
> קורא הייקו על חרא
>
> אהבתי
> הגב
>
>     Yuval Langer
>     2 בנובמבר 2021 ב- 19:27
>
>     היה נדמה לי שכתבתי כאן תיקון לשיר שכתבתי לפני כשנה. נראה שנמחק. מעציב.
>
>     המקורי הוא 7-5-7 במקום 5-7-5, דבר שניסיתי לתקן. אולי תיקון הייקו נחשב כפסול במסורת היפנית ולכן נמחק?
>
>     אהבתי
>     הגב
>         Yuval Langer
>         2 בנובמבר 2021 ב- 19:29
>
>         אם הייקו נכתב על רגע אחד בר חלוף בהכרה, אז הייקו שנכתב הוא מין תמונה של הרגע. אז אולי כל ניסיון לתקן את ההייקו הוא פסול?
>
>         אהבתי
>
> ירח חסר
> 3 בנובמבר 2021 ב- 12:14
>
> איננו יודעים לאן נעלם השיר. מתנצלים. מכל מקום אין בעיה בתיקון הייקו מפני שלא הרגע קובע בלעדית אלא חוויית הרגע והיא נזילה. אפשר לחזור לרגע ולחוותו מחדש, אחרת.
> אגב, השיר שמופיע בהערה כאן לא מוצלח, לדעתנו. בלי קשר למספר ההברות. השורה השניה לא עוברת בעברית והייקו על הייקו זאת התחכמות לא רצויה שהרי לא ברור מה נאמר בשיר הנקרא בשיר. גם הצואה לא מצילה את השיר.
>
> אהבתי
> הגב

In essence, according to ירח חסר, the author of the site, (whose name is literally "missing moon" but maybe "new moon" in colloquial English(?)):

(paraphrasing in haiku syllabic structure, and moment of consciousness stuff (maybe?) but without seasonal stuff)

> considering this
> haiku - form may be valid,
> but is still kaka.

I like this turn of events.
