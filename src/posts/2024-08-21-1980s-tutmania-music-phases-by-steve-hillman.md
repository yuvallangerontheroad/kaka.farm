title: 1980s Tutmania Music - Phases by Steve Hillman!
date: 2024-08-21 00:46
tags: egyptian, music, phases, stevehillman, synthesiser, synthesizer, tutmania
---

Some more magical sounding synthesisers, now from Steve Hillman!

First I listened to his earlier album, [New Horizons][newhorizons], which is deliciously synthetic.

Now listening to Phases!

- [Phases side one part one][phasessideonepartone].
- [Phases side one part two][phasessideoneparttwo].
- [Phases side two part one][phasessidetwopartone].
- [Phases side two part two][phasessidetwoparttwo].

And, oh boy!  Happy days and jubilations!  He has more stuff on [his official Youtube channel][ethericsound]!

[newhorizons]: https://www.youtube.com/watch?v=DbenoyT8Ibk
[phasessideonepartone]: https://www.youtube.com/watch?v=6jBaRrv8FY4
[phasessideoneparttwo]: https://www.youtube.com/watch?v=0UCSOJQ7AOY
[phasessidetwopartone]: https://www.youtube.com/watch?v=CqDvakddmXc
[phasessidetwoparttwo]: https://www.youtube.com/watch?v=pMOFT-Q8uRw
[ethericsound]: https://www.youtube.com/@EthericSound
