title: Handling Python/GTK's GtkScale value-changed event.
date: 2022-01-29 00:00
---

It took me way too long, but now I know how to handle the value-changed event of GtkScale.

In Glade:

1. Create a GtkScale widget. We will ID it `foo_scale`.
2. In the `Signals -> GtkRange -> value-changed` line change the `Handler`
    field to something sensible like `on_foo_scale_value_changed`. That will
    be the name of the handler.
3. Make a new Adjustment in:

    ```
    General ->
    Adjustment ->
    (little pen button in the field) ->
    Choose a [sic] Adjustment in this project ->
    New
    ```

4. Rename the adjustment to something sensible like `foo_adjustment` and set
    the `Value`, `Minimum Value`, `Maximum Value`, etc. Those are kind of self
    explanatory…
5. In Python, your handler will be called with a single argument - the signal
    emitting object, which is the GtkScale object itself. It has a method,
    `get_value()`, used to get its current value:

    ```python
    def on_foo_scale_value_changed(emitter):
        scale_value = emitter.get_value()
	do_stuff_with_scale_value(scale_value)
    ```

Fin.
