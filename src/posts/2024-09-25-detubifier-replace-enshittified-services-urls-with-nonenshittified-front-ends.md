title: Detubifier - Replace Enshittified Services' URLs With Nonenshittified Front Ends.
date: 2024-09-25 10:20
tags: crapsaccharine, crapsaccharinedystopia, elisp, emacs, enshittification, farside, farsidelink, frontends, gnu, gnuemacs, iwanttodisappearcompletely, web
---

I am writing an Emacs package called [detubifier].

It is dedicated to replacing the enshittified URLs of social media services, e.g. youtube or reddit, with their nonenshittified free software front ends, e.g. <https://invidious.io/> or <https://farside.link/>.

I am not clear on the API and just adding functions, like a wrapper over `browse-url`, another function which changes URLs in the kill ring or a marked region.  Just chucking her in the ute 🛻.  There's heaps of space.

Also, I assume I am reinventing this particular sort of wheel, and it is coming out as a [squircle].  I shall reuse this joke in the future.

[detubifier]: https://codeberg.org/kakafarm/emacs-detubifier
[squircle]: https://en.wikipedia.org/wiki/Squircle
