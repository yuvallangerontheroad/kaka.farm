title: Random E-Music Off A Double Pendulum Video.
date: 2024-10-11 16:54
tags: electronicmusic, emusic, music, pretty, prettythings, programmingpart
---

# Nice e-musics found in a pretty double pendulum video:

## The musiques:

Stephen Keech - The Void - Digital Abyss:

<https://www.youtube.com/watch?v=ECN-3MydHtg>

Out of Flux - cdHiddenDir:

<https://www.youtube.com/watch?v=9tDtOa_z1bc>

Yehezkel Raz - Fata Morgana - Instrumental Version:

<https://www.youtube.com/watch?v=9AkwjNrzcMI>

## The Double Pendulums:

Order Within Chaos in the Double Pendulum (Island of Stability Simulation):

<https://www.youtube.com/watch?v=OIHyN7TzY6A>
