title: I Have Renamed "Fancy FFAP Menu" To "Fancy URLs Menu".
date: 2024-11-26 06:03
tags: elisp, emacs, ffap, gnu, gnuemacs, package, urls
---

I have renamed [`fancy-ffap-menu`](https://codeberg.org/kakafarm/emacs-fancy-ffap-menu/) to [`fancy-urls-menu`](https://codeberg.org/kakafarm/emacs-fancy-urls-menu/), as this new name has a bit more what-it-says-on-the-tin-ness.  Relevant discussion is on [the MELPA pull request](https://github.com/melpa/melpa/pull/9253).

Oh well. That's about that.
