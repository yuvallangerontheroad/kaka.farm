title: The Globally Homogenised Fashion Industry And Its Discontents... And Its Contents.
date: 2024-09-20 22:40
tags: fashion, globalhomogenisation, jerryseinfeld, rant, raygungothic, sciencefiction, standupcomedyisphilosophy
---

Manning Publications is a publishing house dedicated to books about information technology ([Wikipedia][wpmanning]).

Its book covers feature olde traditional costumes from across the world ([Manning Catalogue][catalogue], [Wikipedia][wpmanningbranding]).

Now, isn't it a shame we've lost this dazzling variance to our globally homogenised uniform of a pair of jeans and t-shirt?  All this cultural richness we've seen a mere 100 years ago - **gone**.  Tsk tsk.

On the flipside to this genuine and heartfelt lament, Seinfeld [has a bit][bit] in his [I'm Telling You for the Last Time][imtellingyou] comedy special:

> Clothing to me, for the most part, is just such a tremendous pain in the ass.
> 
> If you think of the amount of time, mental effort, physical energy, that goes into your clothes:
> 
> Picking'em, buying'em, does that go with that I don't think I can wear that, I'm missing a button, this is dirty, I gotta get something new, that's up my ass, can't wear this...
> 
> I think we should all wear the same exact clothes.
> 
> Because it seems to be what happens eventually, anyway.
> 
> Anytime you see a movie or a TV-show where there's people from the future or another planet they're all wearing the same outfit.
> 
> I think the decision just gets made:
> 
>> All right, everyone, from now on, it's just gonna be the one piece silver suit with the V stripe and the boots.
>> 
>> That's the outfit.
>> We're gonna be visiting other planets, we wanna look like a team here.
>> The individuality thing is over.

I would add to that that another good thing about these massively mass producing space faring globally homogenised fashion industry civilisation is that, in order to have a change of wardrobe, you do not need to get yet another mortgage or sell one of your kids to indentured servitude…  maybe someone else's in a distant land………  to a [sweatshop][sweatshops]……… ……… ………  I don't know, man.  Out of sight out of mind, eh?

A jeans and a t-shirt.  That's how we will present to the Pleiadeans.

Well, that's about that.  Bye.

    🐮
    👕
    👖


[bit]: https://www.youtube.com/watch?v=2GO3X6RXIvs&t=3151s
[catalogue]: https://www.manning.com/catalog
[imtellingyou]: https://en.wikipedia.org/w/index.php?title=I%27m_Telling_You_for_the_Last_Time&oldid=1244120743
[princeofvibes]: https://www.youtube.com/watch?v=idYERhOsw54
[sweatshops]: https://en.wikipedia.org/w/index.php?title=Sweatshop&oldid=1234774540#Debate_over_the_effects_of_globalization_and_sweatshops
[wpmanning]: https://en.wikipedia.org/w/index.php?title=Manning_Publications&oldid=1244867916
[wpmanningbranding]: https://en.wikipedia.org/w/index.php?title=Manning_Publications&oldid=1244867916#Branding
