title: The Night Land by William Hope Hodgson.
date: 2024-05-13 06:14
tags: israel, sciencefiction, doom, antitheism
---

The Night Land by William Hope Hodgson is a science fiction tale about the very far future, when the sun had died out and the last remnants of Mankind live in a gigantic pyramid called The Last Redoubt.

Their existence dependent on ebbing geothermal energy and a force field to protect them from whatever lurks beyond - outside the pyramid is almost certain death, or fates much worse.

Around the pyramid patiently stand beings of unimaginable power, always watching into the pyramid, waiting for the Earth Current, for the force field to fail, for the humans to fall.

That's Israel in the Middle East.

That's The Night Land at The Holy Land.
