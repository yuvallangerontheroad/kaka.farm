date: 2024-01-28 09:21
title: Good Burger and Brothers Grimsby
tags: comedy, films, review
---

Yesterday I watched a dumb comedy film with Mother.  It was a about a
burgers restaurant filled with a cast of weird employees.  Good
Burger, it is called.  I am ambivalent about it.  On one hand it has a
co-star character which made me uncomfortably embarrassed with his
literal interpretations of common American English idioms, e.g. when
he is told to "watch your butt" and he would start going in circles,
like a dog chasing his own tail.  On the other hand, it made me
chuckle a few times.  It was also fairly cheerful, colourful and had a
sense of optimism which I find both alien and endearing.  A film for
kids, all in all, but it was okay.

The night before or so, though, we watched Grimbsby, which was a
nonstop laughter Sach Baron Cohen shock comedy.  I had already watched
it several years ago, but the comedy did not diminish.
