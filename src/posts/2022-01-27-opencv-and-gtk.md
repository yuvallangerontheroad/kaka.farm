title: Finding mouse location in Python + GTK + OpenCV.
date: 2022-01-27 00:00
---

Expanding on the tutorial:

<https://nofurtherquestions.wordpress.com/2017/03/03/python-3-5-gtk-3-glade-and-opencv/>

it originally had shown the following structure:

```
- window1 (GtkWindow)
    - box1 (GtkBox)
        - grayscaleButton (GtkToggleButton)
        - image (GtkImage)
```

According to <https://stackoverflow.com/a/24388310/189995> `GtkImage` does not
generate events, so if you want to find the mouse location, put your
`GtkImage` inside a `GtkEventBox`:

```
- window1 (GtkWindow)
    - box1 (GtkBox)
        - grayscaleButton (GtkToggleButton)
        - eventbox (GtkEventBox)
            - image (GtkImage)
```

and in `eventbox` set the `Common -> Events -> Pointer Motion` so you can add
a handler to the `motion-notify-event` event.

Now you can write your handler:

```python
class Handler:
    …
    …
    …
    def onMotionNotifyEvent(*args):
        global current_mouse_location
    
        event = args[1]

        current_mouse_location = event.x, event.y
```
