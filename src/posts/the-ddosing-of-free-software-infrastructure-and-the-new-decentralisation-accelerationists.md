author: Yuval Langer
date: 2024-01-12 01:14
tags: codeberg, sourcehut, ddos, fossil
title: The DDoSing of Free Software Infrastructure and The New Decentralisation Accelerationists
---

(no, this is not a serious post.  I don't believe any of this, none
except that Fossil's probably good for you.)

As I am writing these words, [Codeberg][1] and [Source Hut][2] are
under a [Distributed Denial of Service][3] attack from an unknown
party.

People are speculating about the source of the attack.  Who would want
to cause such a serious disruption to Free Software development and
dissemination?  An attack that is about as vile as kicking a baby for
laughing too loudly.

Some say proprietary software vendors want to disrupt Free Software
and draw people towards their walled gardens.  Some say it's the
proprietary Git "forges" who would like to draw the scared masses and
their software to their centralised services.  Both of those parties
locking the walls behind the new inhabitants.  Others say it's surely
the [Content Delivery Network][4] services, dabbling in an information
technology [protection racket][5].

No, those who would have the most to profit from this attack are the
decentralisationists.  A rogue team of decentralisation
[accelerationists][6] want us to be miserable on Codeberg and Source
Hut, and to run our own instances of software such as [Fossil][7],
which have all of the benefits of Git, tracking code, but also a wiki,
and issues, all in an entirely decentralised way, no need for a Git
forge.

[1]: https://codeberg.org/
[2]: https://sr.ht/
[3]: https://en.wikipedia.org/wiki/Denial-of-service_attack#Distributed_DoS_attack
[4]: https://en.wikipedia.org/wiki/Content_delivery_network
[5]: https://en.wikipedia.org/wiki/Protection_racket
[6]: https://en.wikipedia.org/wiki/Accelerationism
[7]: https://www.fossil-scm.org/
