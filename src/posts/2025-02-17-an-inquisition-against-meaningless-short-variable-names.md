title: An Inquisition Against Meaningless Short Variable Names.
date: 2025-02-17 16:08
tags: gnuguile, guile, lisp, programming, scheme
---

I have started an inquisition.

An inquisition against meaningless short variable names.

Putting each of them on the rack and pulling until they make sense and put a skewer up their bum - from `var` to `meaningful-many-words-shishlik-style-variable-name`. 🍢

For example, in [guile-bytevector-peg](https://codeberg.org/kakafarm/guile-bytevector-peg/) (as of now just a work in progress):

- `accum` is now `accumulation-type`,
- `pat` is `pattern`, and
- `plen` is `pattern-length`.

NO SOURCE FILE SHALL BE LEFT UNTOUCHED.

NO VARIABLE NAME WILL GO UNEXAMINED.

YOUR MEANINGLESS EXISTENCE IS OVER! 💀
