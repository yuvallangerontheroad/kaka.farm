author: Yuval Langer
title: Comics Idea: A Typical Offtopic IRC Channel Experience.
date: 2023-11-23 13:59
---

0. A typical livingroom with a bunch of friends talking and whatnot.
1. Knock on the door. Everyone are looking at the door.
2. Someone opened the door. It's a guy with an origami paper hat and a
   stained smudged shirt, black beneath his eyes.
4. Walks in, poops on the floor, and writes walls of text on the
   walls.
5. One guy looks in disgust and another one says to the disgust guy
   "Just /ignore him."
6. Underneath is a caption written, "a typical offtopic IRC channel".
