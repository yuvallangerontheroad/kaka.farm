title: You Can Stop Companies From Killing Games
date: 2024-07-31 21:06
tags: consumerprotection, consumerrights, europeanunion, accursedfarms, computergames, euro, europe, games, rossscott, stopkillinggames, videogames
---

From <https://www.stopkillinggames.com/eci> :

> Videogames are being destroyed! Most video games work indefinitely,
> but a growing number are designed to stop working as soon as
> publishers end support. This effectively robs customers, destroys
> games as an artform, and is unnecessary. Our movement seeks to pass
> new law in the EU to put an end to this practice. Our proposal would
> do the following:
>
> - Require video games sold to remain in a working state when support
>   ends.
> - Require no connections to the publisher after support ends.
> - Not interfere with any business practices while a game is still
>   being supported.
>
> If you are an EU citizen, please sign the Citizens' Initiative!

Again, <https://www.stopkillinggames.com/eci> .

(you may also bug any European around you, or any person around you
that knows any Europeans, to sign this petition)

The pitch video explaining all of this:

<https://www.youtube.com/watch?v=mkMe9MxxZiI>

Good luck.
