title: Ecclesiastes, John, Kobayashi Issa, and Peter Watts plagiarism.
date: 2024-12-03 12:36
tags: haiku, john, kobayashiissa, peterwatts, plagiarism, scripture
---

In reply to <https://shitposter.world/notice/AoeGyWzd9Vfx5V6FI8> I wrote something like:

The Flesh made Words and the words made their dwelling among us.  And the
words reasoned it all as "The world of dew, is just a world of dew"
evaporating into "vapour, all is vapour" in the first rays of morning sun, but
the Flesh was still suffering with its hundred million years of limbic
evolution, and there came a reply by the Flesh manifest as Words - "and
yet..."

- [Peter Watts](https://rifters.com/real/shorts/PeterWatts_Flesh.pdf).
  - <https://rifters.com/>.
  - <https://en.wikipedia.org/wiki/Peter_Watts_(author)>.
- [Kobayashi Issa](https://www2.cs.arizona.edu/~kece/Personal/Poems/issa.html).
  - <https://www.lionsroar.com/about-a-poem/>.
  - <https://allpoetry.com/a-world-of-dew>.
  - <https://en.wikipedia.org/wiki/Kobayashi_Issa>.
- [Ecclesiastes](https://www.skepticsannotatedbible.com/ec/1.html).
- [John](https://www.skepticsannotatedbible.com/jn/1.html).
