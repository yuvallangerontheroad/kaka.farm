date: 2024-05-07 04:31
title: Spring 2024 Lisp Game Jam.
tags: games, gamdev, lisp, scheme, guile, hoot, guile-hoot, chicken, spock
---

The [Spring Lisp Game Jam 2024] is next week, and I have an idea,
thanks to a <ircs://libera.chat/lispgames> user nicknamed aeth.  It
will be a game about an adventuring swordmaster rogue who is currently
on a prolonged vacation back in his house and is now leisurely solving
sudokus while drinking lots of cocoa coffee.  I'll call it Sudokus
Swordmaster, or "סודו[כוס החרבות]" in Hebrew.  (the original idea was
some pointless game where you're cutting enemies up with a sword, but
this one has an extra layer of pointless narrative, which I find
funny)

Last time I wrote [a terrible falling bricks evasion game].  I only
really worked on it when the time was nearly up, but it still managed
to win first place (from the bottom).  It was written using the [Spock
Scheme dialect], a Scheme to Javascript compiler, which itself is
written in [Chicken Scheme], a Scheme to C (and then native
executable) compiler.

[Spring Lisp Game Jam 2024]: https://itch.io/jam/spring-lisp-game-jam-2024

[כוס החרבות]: https://he.wikipedia.org/wiki/%D7%9B%D7%95%D7%A1_%D7%94%D7%97%D7%95%D7%A8%D7%91%D7%95%D7%AA

[a terrible falling bricks evasion game]: https://itch.io/jam/autumn-lisp-game-jam-2023/rate/2339376

[Spock Scheme dialect]: https://wiki.call-cc.org/eggref/5/spock

[Chicken Scheme]: https://call-cc.org/
