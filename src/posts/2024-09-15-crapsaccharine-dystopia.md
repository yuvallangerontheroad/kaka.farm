title: A Few Anecdotes From Our Crapsaccharine Dystopia.
date: 2024-09-15 11:47
tags: asimov, crapsaccharine, dystopia, foundation, technopriests
---

A friend wrote two [crapsaccharine dystopia][csw] anecdotes:

> I asked someone recently to download a file on their phone and I gave them a link to the file.
> They couldn't download the file because they kept pasting the link into their search engine application.
> I had told them to use their browser.
> Then later I asked them if they knew what an "application" is and they said "not really".
> I read online that a teacher in an elementary school handed out some textbooks to their new class of students, and the students were using their fingers to try to stretch the pictures bigger in their textbooks.
> I think in the future people won't be able to distinguish between technology and magic

That last remark reminds me too strongly of the [Foundation] by Isaac Asimov.

[csw]: https://tvtropes.org/pmwiki/pmwiki.php/Main/CrapsaccharineWorld
[Foundation]: https://en.wikipedia.org/wiki/Foundation_(Asimov_novel)
