title: GNU Emacs Advice Function Removal Before Dawn
date: 2024-07-12 04:38
tags: elisp, emacs, gnu, lisp
---

The following function definition removes every advice-function from
an advised-function.

```lisp
(defun kakafarm/advice-unadvise (function)
  "Remove every advice-function from FUNCTION."
  (advice-mapc
   (lambda (advice-function properties-alist)
     (advice-remove function
                    advice-function))
   function))
```

Let's say you add the following advice to the `scratch-buffer` command
so that the `*scratch*` buffer would switch to text-mode each time you
run `scratch-buffer`:

```lisp
(advice-add 'scratch-buffer
            :after
            (lambda ()
              (text-mode)))
```

Maybe you also add more advice functions interactively to the
`scratch-buffer` function it's a big mess.  You want to remove all the
advice functions, so you run:

```lisp
(kakafarm/advice-unadvise 'scratch-buffer)
```