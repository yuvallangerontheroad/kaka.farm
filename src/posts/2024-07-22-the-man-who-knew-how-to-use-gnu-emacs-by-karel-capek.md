date: 2024-07-22 08:44
title: The Man Who Knew How To Use GNU Emacs by Karel Čapek.
tags: emacs, fiction, gnu, gnu-emacs, karel-capek, live-love-life, muscle-memory, pura-vida, recommendation, short-story, story, story-recommendation, technomancy
---

Trying to recall which exact keybindings I regularly press when firing
commands feels like explaining how to fly in this very short story by
[Karel Čapek][karelcapek] called [The Man Who Knew How To
Fly][themanwhoknewhowtofly].  It's a good story, so give it a read!

[themanwhoknewhowtofly]: https://tzin.bgu.ac.il/~arikc/fly.html "The Man Who Knew How To Fly on Dr. Ariel (Arik) Cohen's Ben Gurion University personal page."
[karelcapek]: https://en.wikipedia.org/wiki/Karel_%C4%8Capek "The author's page up on the Wikipedia"
