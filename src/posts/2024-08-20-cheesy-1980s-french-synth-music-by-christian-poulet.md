title: Cheesy 1980s French Synth Music by Christian Poulet!
tags: cheesy, french, music, playlist, pleasant, synthesiser, synthesizer
date: 2024-08-20 23:19
---

These compositions by French compositor Christian Poulet, [Le Voyage Imaginaire (1988)][levoyage] are quite pleasant.  Slow synthesiser melodies of retrofuturisms.  You might want to read some 1980s Hugo Prize short story winners to it, like Press Return or Blood Music, maybe also Masamune's Appleseed and Ghost In The Shell.

[levoyage]: https://www.youtube.com/watch?v=ZlPk39DtxrY
