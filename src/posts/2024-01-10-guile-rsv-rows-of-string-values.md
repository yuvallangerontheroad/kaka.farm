title: guile-rsv - A Rows of String Values implementation.
tags: guile, csv, tsv, rsv
date: 2024-01-10 01:22
---

I had a video about ["Rows of String Values"][1] automatically
recommended to me and I decided to [implement it for myself][2] as an
exercise following [the specification][3]:

[1]: https://www.youtube.com/watch?v=tb_70o6ohMA
[2]: https://codeberg.org/kakafarm/guile-rsv
[3]: https://github.com/Stenway/RSV-Specification
