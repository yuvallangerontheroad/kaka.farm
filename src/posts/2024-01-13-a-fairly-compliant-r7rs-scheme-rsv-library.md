title: A fairly compliant R7RS Scheme RSV (Rows of String Values) library.
tags: guile, csv, tsv, rsv, r7rs, scheme
date: 2024-01-13 10:21
---

A few days ago [I wrote][1] about writing a [Guile library][2] for
reading and writing [RSV files][3].  I have reached a point where the
tests all succeed and the code is seems to be R7RS standard compliant
Scheme.  All recommendations, criticism, hints, arcane secrets, and
spiritual advice would be welcome - I go by "cow_2001" on the #scheme
IRC channel on [Libera Chat][4].

[1]: https://kaka.farm/posts/guile-rsv---a-rows-of-string-values-implementation.html
[2]: https://kaka.farm/~stagit/guile-rsv/log.html
[3]: https://github.com/Stenway/RSV-Specification
[4]: https://libera.chat/
