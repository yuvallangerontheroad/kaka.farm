title: Lisping Buffalo.
date: 2024-07-26 09:51
tags: compulsion, brianlimmylimond, daftlimmy, limmy, lisp, sorry
---

Watching [Brian "Daft Limmy" Limond (the "steel is heavier than
feathers" guy) trying to figure out the "Buffalo buffalo Buffalo
buffalo buffalo buffalo Buffalo buffalo." sentence][daftlimmy], I had
a compulsion to copy it down to Lisp.  I don't even know what it
means.  I am so sorry.

```lisp
'(S (NP (NP (PN "Buffalo")
         (N "buffalo"))
     (RC (NP (PN "Buffalo")
             (N "buffalo"))
      (V "buffalo")))
  (VP (V "buffalo")
   (NP (PN "Buffalo")
    (N "buffalo"))))
```

[daftlimmy]: https://www.youtube.com/watch?v=Z4GJ7T6qBcU
