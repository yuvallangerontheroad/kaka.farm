date: 2024-05-27 12:09
title: What Is The Name Of This Science Fiction Short Story?
tags: forgotten, sciencefiction, scifi, shortstories, books
---

Forgotten the name of this story and I need help finding it again.

It goes something like this (spoilers ahead):

The story is about a man and his family visiting a resort close to nature.  The resort has a series of missing people.  The man talks with an old man who is there for fishing, and it seems like this is the only thing the old man is talking about.  At one point the man notices tin cans and milk cartons labelled strangely, as if the one labelling them doesn't really know what the labels mean.  When visiting the cupboard or the refridgerator he is sucked into a vortex from another dimension, but narrowly escapes it.  In the last page or so the old man is talking about his fishing decoy and the one that got away, making us realise that there are aliens there using strange tin cans and milk cartons as decoy and that our protagonist was the one that got away.

If you know, please send an e-mail to yuval.langer@gmail.com, message me on IRC (efnet, libera.chat, quakenet, or rizon) to nickname cow_2001, or on whatever part of the balkanised Activitypub federation that can send to @kakafarm@shitposter.world (previously @kakafarm@emacs.ch).
