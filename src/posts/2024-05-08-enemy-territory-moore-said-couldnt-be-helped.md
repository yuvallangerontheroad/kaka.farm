title: “Enemy territory,” Moore said. “Couldn’t be helped.”
date: 2024-05-08 02:08
tags: blindsight, csj, echopraxia, peterwatts, scifi
---

First published in [@kakafarm@emacs.ch] 2024-04-20T14:39Z.

Whenever the colonial army of the CSJ Empire take over another swath
of land within the Infosphere in which I inhabit, I am reminded of a
line from the novel Echopraxia by Peter Watts.  They forever leave
behind something precious and the simple terrible phrase uttered by
their military strategist:

> “Enemy territory,” Moore said. “Couldn’t be helped.”

Everyone, burn the bridges behind you.  There is no way back home.

[@kakafarm@emacs.ch]: https://emacs.ch/@kakafarm/112304059948019561
