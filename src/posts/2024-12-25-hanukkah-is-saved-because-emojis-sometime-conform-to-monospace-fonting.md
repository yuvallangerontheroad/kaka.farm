title: Hanukkah Is Saved Because Emojis Sometime Conform To Monospace Fonting!
date: 2024-12-25 02:55
tags: emacs, fonts, fullwidthforms, gnuemacs, hanukiyah, hanukkah, hanukkahmenorah, israel, judaism, kaka, khanukah, khanukiyah, monospace, poo, poop
---

I have tried copying the clisp[1] khanukiyah[2], adding some poo on top, and then lighting it on fire for Hanukkah, but the emoji just would not align with the monospace font.

Turns out emoji only align with monospace fullwidth forms[3], and they do not align in GNU Emacs, at least not in my current configuration, hence the previous post about Khanukah being ruined.

Khanukah is saved.  Saved!  You can all go home now.

```
🔥　　🔥　　🔥　　🔥　　🔥　　🔥　　🔥
💩　　💩　　💩　　💩　　💩　　💩　　💩
Ｉ　　Ｉ　　Ｉ　　Ｉ　　Ｉ　　Ｉ　　Ｉ
Ｉ　　　＼　｀－－＋－－＇　／　　　Ｉ
　＼　　　｀－－－＋－－－＇　　　／
　　｀－＿＿＿＿＿｜＿＿＿＿＿－＇
　　　　　　　　　｜
－－－－－－－－－＋－－－－－－－－－
```

[1]: https://www.gnu.org/software/clisp/
[2]: https://en.wikipedia.org/wiki/Hanukkah_menorah
[3]: https://en.wikipedia.org/wiki/Halfwidth_and_fullwidth_forms
