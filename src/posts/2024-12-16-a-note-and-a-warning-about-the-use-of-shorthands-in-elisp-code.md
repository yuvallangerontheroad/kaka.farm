title: A Note And A Warning About The Use Of Shorthands In Elisp Code.
date: 2024-12-16 12:40
tags: autoload, elisp, emacs, emacslisp, gnu, gnuemacs, packages, shorthands
---

[In a previous post][1] I wrote about elisp shorthands and how to properly use them.

A comment on my [MELPA submission for fancy-urls-menu][2] by [riscy][3] (aka [Chris Rayner][4]) a real elisp boffin who knows much more about actual Emacs development than I do says one should avoid shorthands altogether, and he gives very good reasons:

> I'm unsure specifically what issue you refer to, but as a code reviewer I am not currently a huge fan of shorthands. A lot of the tooling is not ready for them yet: not only the linters, but also simple tools for quickly jumping around between definitions, the ability to grep for code using function/variable names, the ability to copy-paste-execute code in other contexts and have it mean the same thing. Or maybe most importantly, the ability to parachute into a particular line of a particular elisp file and comprehend (or at least prove something about) what's going on without a lot of assumed context (which is also a problem with other programming techniques e.g. inheritance and metaprogramming).
>
> I understand where they're useful but I (personally) think it can be selfish for a package author to use them and I'm glad you moved away from them :)

[1]: /posts/do-not-use-shorthands-for-autoloaded-functions.html
[2]: https://github.com/melpa/melpa/pull/9253#issuecomment-2526277066
[3]: https://github.com/riscy
[4]: https://riscy.io/
