title: A Package For Dealing With Lists Of Remote OPML Files And Interfacing Them With Elfeed Feeds
date: 2024-06-29 06:42
tags: atom, elisp, emacs, feeds, gnu, opml, rfc, rss
---

I've written a bunch of functions and commands that retrieve OPML
files and update Elfeed's `elfeed-feeds` with the feeds of those
OPMLs.

https://codeberg.org/kakafarm/emacs-opml-to-elfeed-feeds

The user stores a list of OPML addresses as the custom variable
`opml-to-elfeed-feeds-opml-list`.  Each OPML address optionally tagged
the same way the feeds in `elfeed-feeds` are tagged.

The user then runs a certain command to add the list of feeds
retrieved from `opml-to-elfeed-feeds-opml-list` to `elfeed-feeds`.

Except for `o2e-opml-list` and `o2e-elfeed-feeds` I haven't yet
exposed any API to the user, as in `package-name--foo` versus
`package-name-foo` and `(interactive ...)`.

So what kind of functionality would you find useful for dealing with
remote OPML files?
