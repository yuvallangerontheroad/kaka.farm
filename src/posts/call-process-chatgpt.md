title: ChatGPT and Emacs Lisp
date: 2023-07-07 14:45
---
# ChatGPT explaining Emacs Lisp

A perplexed friend asked ChatGPT what my code does.  I shared it with
a bunch of people and a person on #systemcrafters-general:matrix.org
then suggested I ask ChatGPT for improvements.

## Original

    (progn
      (defun kakafarm/call-process-with-string-as-input (program &optional input-string &rest args)
        (with-temp-buffer
          (let ((our-output-buffer (current-buffer)))
            (if input-string
                (with-temp-buffer
                  (let ((our-input-buffer (current-buffer)))
                    (progn
                      (erase-buffer)
                      (insert input-string)
                      (apply 'call-process-region
                             (buffer-end -1)
                             (buffer-end 1)
                             program
                             nil
                             our-output-buffer
                             nil
                             args))))
              (apply 'call-process
                     program
                     nil
                     our-output-buffer
                     nil
                     args)))
          (buffer-string)))

      (kakafarm/call-process-with-string-as-input "cat"
                                                  "cat says moo")
      (kakafarm/call-process-with-string-as-input "echo"
                                                  nil
                                                  "echo echo echo"))

## Explanation by ChatGPT

    The code defines a function named kakafarm/call-process-with-string-as-input. This function takes three arguments: program, input-string, and args. It executes a command-line program (program) and returns the output as a string. The input-string argument is optional and allows passing input to the program.

    The function starts by creating a temporary buffer using the with-temp-buffer macro. This buffer will be used to store the output of the command. The variable our-output-buffer is bound to the current buffer.

    Next, there is a conditional statement that checks if an input-string is provided. If it is, another temporary buffer (our-input-buffer) is created to hold the input string. The erase-buffer function clears the our-input-buffer, and the insert function inserts the input-string into the buffer.

    Then, the function call-process-region is called with arguments (buffer-end -1) and (buffer-end 1), which represent the beginning and end of the our-input-buffer. This function executes the specified program with the given input and redirects the output to our-output-buffer. The args are also passed to the call-process-region function.

    If no input-string is provided, the function call-process is used instead. It directly executes the program with the given args, without any input.

    After executing the command, the buffer-string function is called to retrieve the contents of the our-output-buffer. This value is then returned as the result of the function.

    Finally, the function is invoked twice to demonstrate its usage. In the first invocation, the program "cat" is executed with the input string "cat says moo". In the second invocation, the program "echo" is executed without any input, but with the arguments "echo echo echo".

    Overall, this code snippet defines a utility function that simplifies calling command-line programs and capturing their output in Lisp. It demonstrates the flexibility of Lisp in handling strings, buffers, and external processes.

## Improvements by ChatGPT
### Code

    (defun kakafarm/call-process-with-string-as-input (program &optional input-string &rest args)
      (if input-string
          (with-temp-buffer
            (insert input-string)
            (apply 'call-process-region (point-min) (point-max) program nil t nil args))
        (apply 'call-process program nil t nil args)))

    (list (kakafarm/call-process-with-string-as-input "cat" "cat says moo")
          (kakafarm/call-process-with-string-as-input "echo" nil "-n" "echo echo echo"))

### Explanation

    Improvements:

    Removed unnecessary progn wrapper around the defun.
    Simplified the logic for handling input strings. No need for nested with-temp-buffer calls and separate buffer variables.
    Replaced (buffer-end -1) and (buffer-end 1) with (point-min) and (point-max) respectively for clarity.
    Replaced (buffer-string) with t in call-process-region to discard the output buffer and avoid unnecessary memory allocation.
    Removed unnecessary nil argument in call-process.
    These improvements make the code cleaner and more efficient.

## Doom?

My friend asked if it make sense. Does it?

### Running the code

    (progn ; Adding a progn so I can run both expressions in one go.
      (defun kakafarm/call-process-with-string-as-input (program &optional input-string &rest args)
        (if input-string
            (with-temp-buffer
              (insert input-string)
              (apply 'call-process-region (point-min) (point-max) program nil t nil args))
          (apply 'call-process program nil t nil args)))

      (list (kakafarm/call-process-with-string-as-input "cat" "cat says moo")
            (kakafarm/call-process-with-string-as-input "echo" nil "-n" "echo echo echo")))
    echo echo echo ; The result after running eval-last-sexp (C-x C-e).

I've expected a string return value of the whole progn expression, yet
what I've got was an insertion to this here buffer.

We are safe...

...

for now.

## Yes, we are safe.

https://chat.openai.com/share/08cc30e5-21ce-4549-9f3b-7de82d40f9b9

Copying it here:

    Shared Chat•
    Model: Default
    Improve Lisp program.
    July 7, 2023
    can you improve this program please?

    (progn
      (defun kakafarm/call-process-with-string-as-input (program &optional input-string &rest args)
        (with-temp-buffer
          (let ((our-output-buffer (current-buffer)))
            (if input-string
                (with-temp-buffer
                  (let ((our-input-buffer (current-buffer)))
                    (progn
                      (erase-buffer)
                      (insert input-string)
                      (apply 'call-process-region
                             (buffer-end -1)
                             (buffer-end 1)
                             program
                             nil
                             our-output-buffer
                             nil
                             args))))
              (apply 'call-process
                     program
                     nil
                     our-output-buffer
                     nil
                     args)))
          (buffer-string)))

      (list (kakafarm/call-process-with-string-as-input "cat"
                                                        "cat says moo")
            (kakafarm/call-process-with-string-as-input "echo"
                                                        nil
                                                        "-n"
                                                        "echo echo echo")))

    ChatGPT

    Certainly! Here's an improved version of the program:

    lisp

    (defun kakafarm/call-process-with-string-as-input (program &optional input-string &rest args)
      (if input-string
          (with-temp-buffer
            (insert input-string)
            (apply 'call-process-region (point-min) (point-max) program nil t nil args))
        (apply 'call-process program nil t nil args)))

    (list (kakafarm/call-process-with-string-as-input "cat" "cat says moo")
          (kakafarm/call-process-with-string-as-input "echo" nil "-n" "echo echo echo")))

    Improvements:

        Removed unnecessary progn wrapper around the defun.
        Simplified the logic for handling input strings. No need for nested with-temp-buffer calls and separate buffer variables.
        Replaced (buffer-end -1) and (buffer-end 1) with (point-min) and (point-max) respectively for clarity.
        Replaced (buffer-string) with t in call-process-region to discard the output buffer and avoid unnecessary memory allocation.
        Removed unnecessary nil argument in call-process.

    These improvements make the code cleaner and more efficient.
    I've expected a string return value of the whole progn expression, yet
    what I've got was an insertion to this here buffer.
    ChatGPT

    Apologies for the misunderstanding. If you would like the whole progn expression to return a string instead of inserting it into a buffer, you can use the with-output-to-string macro. Here's the updated code:

    lisp

    (progn
      (defun kakafarm/call-process-with-string-as-input (program &optional input-string &rest args)
        (with-output-to-string
          (if input-string
              (with-input-from-string input-string
                                      (apply 'call-process-region program nil standard-output nil args))
            (apply 'call-process program nil standard-output nil args))))

      (kakafarm/call-process-with-string-as-input "cat" "cat says moo"))

It had imagined a new form called
`with-input-from-string`. It does not work.

    In this version, the with-output-to-string macro captures the output of the progn expression and returns it as a string. The standard-output stream is used as the output destination for the call-process-region or call-process functions, depending on whether an input string is provided.

    Please note that the (kakafarm/call-process-with-string-as-input "echo" nil "-n" "echo echo echo") part from your original code has been removed since it wasn't necessary for demonstrating the changes. Feel free to add it back if needed.
    Continue this conversation
    |
    Terms of use
    |
    Privacy policy

    Improve Lisp program.
