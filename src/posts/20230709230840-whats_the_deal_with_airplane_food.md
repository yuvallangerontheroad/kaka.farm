title: What **is** the problem with airplane food?
author: Yuval Langer
date: 2023-07-09 00:00
---

The last time I travelled by aeroplane was many years ago.  I do not
remember the food served on those flights - not its texture, not its
colour, not by taste.  Being anosmic, I remember it least by smell.
The only thing I can do is look up recorded accounts of those who have
stepped forward and came out publicly to talk about the problem.

The list:

- [It tastes bad](https://youtu.be/AsJYmf_G5d0?v=40s).
- [The almonds bag is too small](https://youtu.be/AsJYmf_G5d0?v=45s).
- [The peanuts bag is too small](https://youtu.be/n0E7EaRLmSI?v=45s).
- [It costs too much and is not gormet food](https://youtu.be/cbjyxcfM2XI?t=37s).
