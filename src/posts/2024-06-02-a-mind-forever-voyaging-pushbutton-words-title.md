title: A Mind Forever Voyaging: A Pushbutton Words Title
date: 2024-06-02 16:48
tags: interactivefiction
---

Turns out the title "[A Mind Forever Voyaging][1]" came from [William
Wordsworth][2]'s excruciatingly long poem [The Prelude][3].  I don't
know if the computer game can live up to such a Pushbutton Words
title.

From the Turkey City Lexicon, as shown on [TVTropes][4]:

> Pushbutton Words: Words used to evoke a cheap emotional response
> without engaging the intellect or the critical faculties.  Commonly
> found in story titles, they include such bits of bogus lyricism as
> "star," "dance," "dream," "song," "tears" and "poet," cliches
> calculated to render the SF audience misty-eyed and tender-hearted.

[1]: https://www.ifwiki.org/A_Mind_Forever_Voyaging
[2]: https://en.wikipedia.org/wiki/William_Wordsworth
[3]: https://en.wikisource.org/wiki/The_Prelude_(Wordsworth)
[4]: https://tvtropes.org/pmwiki/pmwiki.php/Website/TurkeyCityLexicon
