title: First GNU Guix Contributions
author: Yuval Langer
tags: guix, gnu
date: 2024-01-05 22:36
---

This is rather exciting!  A few hours ago I've gotten two emails from
[the bug tracking system of the GNU project][1], one for the
[guile-srfi-133][2] and the other for the [guile-srfi-232][3] package
definition patches I wrote.  They've been tweaked and added to the
[GNU Guix][4] distribution by [Ludovic Courtès][5] as two commits, one
for [guile-srfi-133][6] and one for [guile-srfi-232][7].

I was worried my contribution was not good enough and was ignored by
the developers because I first sent these patches a few weeks ago………
I still have an [unmerge package definition][8] for [Michelangelo
Rodriguez's greader][9], but now I see that it might be merge
eventually.

[1]: https://debbugs.gnu.org/
[2]: https://debbugs.gnu.org/cgi/bugreport.cgi?bug=67847
[3]: https://debbugs.gnu.org/cgi/bugreport.cgi?bug=67760
[4]: https://guix.gnu.org/
[5]: https://www.wikidata.org/wiki/Q15981411
[6]: https://git.savannah.gnu.org/gitweb/?p=guix.git;a=commitdiff;h=5dbfd2db62354d96eb69d30a66422f362688ebbb
[7]: https://git.savannah.gnu.org/gitweb/?p=guix.git;a=commitdiff;h=5279bd453f354cbbaafff44e46c6fa03a39bc10a
[8]: https://debbugs.gnu.org/cgi/bugreport.cgi?bug=68116
[9]: https://elpa.gnu.org/packages/greader.html
