title: EWW Is Not Eww (also, info books are great)
date: 2022-11-21 00:00
author: Yuval Langer
tags: clipboard-speaker, emacs, eww
---

So I find it hard to read. Yes, some people are like that, but I have
developed a technique to aid me on my readings: (Only for English, not
for my Mother Tongue Hebrew, but that's another topic).

It is all based on a little Python script I wrote called [Clipboard
Speaker][1].

I mark a bunch of text, copy the text, and press a keybinding which
starts the script. The script then reads the contents of the clipboad,
then gives them to a text-to-speech engine that reads it. There are
some additional minor complexities, but that is the main workflow.

Now let me expand on that.

The three keybindings I have set are, again, defined through the Gnome
configuration UI:

- super-c - Takes the regular clipboard which is filled by usually
  pressing C-c.
- super-x - Takes the weird clipboard which is normally filled just by
  marking text.
- super-z - Stops the text-to-speech engine.

Normally you would like to mark a bunch of text and press super-x,
without first pressing C-c and then super-c, but sometimes that does
not work. I do not know why, as in the case with Firefox.

As an aside, Firefox has a Reader Mode which also has built-in
text-to-speech functionality. It is admirable and I love that Mozilla
chose to implement it, but I find it limited - one cannot control
where each batch of text to-be-read starts and where it
finishes. Usually whole paragraphs are chosen. Sometimes one wants to
sit back while the text is zoomed in, but the paragraph slides off the
viewport, so one cannot follow the text below a certain point.

Anyway, I want to read in Firefox, so I mark a bit of text, then press
C-c (text copied to clipboard) s-c (script takes clipboard contents,
starts the TTS engine and passes the text). I mark text, C-c s-c, mark
text, C-c s-c.

Now, if I read something in eww, I have a cursor by default and I can
comfortably move the point to the start of bit of text I want to read,
press C-<SPC>, move point to the end of the bit of text, then press
s-x. Easy.

By the way, a fancy thing I have added to the script's functionality,
is that the text-to-speech engine keeps receiving new text in its
stdin every time I press s-x or s-c, so you can mark text, press s-x
or C-c s-c several times and each of those bits of text would be
played in order.

[1]: https://codeberg.org/kakafarm/clipboard-speaker
