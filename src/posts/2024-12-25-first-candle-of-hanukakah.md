title: First Candle of Hanukakah.
date: 2024-12-25 18:18
tags: hanukakah, hanukkah, israel, judaism
---

First candle of Hanukakah! :D

```
　　　　　　　　🔥　　　　　　　🔥
　　　　　　　　💩　　　　　　　💩
Ｉ　Ｉ　Ｉ　Ｉ　Ｉ　Ｉ　Ｉ　Ｉ　Ｉ
Ｉ　Ｉ　　＼　｀＋＇　／　　Ｉ　Ｉ
Ｉ　　＼　　｀－＋－＇　　／　　Ｉ
　＼　　｀－＿＿｜＿＿－＇　　／　
　　｀－－＿＿＿｜＿＿＿－－＇　　
　　　　　　　　｜　　　　　　　　
－－－－－－－－＋－－－－－－－－
```

I have copied the ASCII art as shown in CLISP (<https://www.gnu.org/software/clisp/>).

```
$ clisp
        i
. . . . I . . . i     ooooo    o        ooooooo   ooooo   ooooo
I I I I I I I I I    8     8   8           8     8     o  8    8
I I  \ `+' /  I I    8         8           8     8        8    8
I  \  `-+-'  /  I    8         8           8      ooooo   8oooo
 \  `-__|__-'  /     8         8           8           8  8
  `--___|___--'      8     o   8           8     o     8  8
        |             ooooo    8oooooo  ooo8ooo   ooooo   8
--------+--------

Welcome to GNU CLISP 2.49.92 (2018-02-18) <http://clisp.org/>

Copyright (c) Bruno Haible, Michael Stoll 1992-1993
Copyright (c) Bruno Haible, Marcus Daniels 1994-1997
Copyright (c) Bruno Haible, Pierpaolo Bernardi, Sam Steingold 1998
Copyright (c) Bruno Haible, Sam Steingold 1999-2000
Copyright (c) Sam Steingold, Bruno Haible 2001-2018

Type :h and hit Enter for context help.

[1]>

Bye.
```

Then I used <https://www.lddgo.net/en/string/full-half-width-converter> to convert the text art from halfwidth to fullwidth, then I have `M-x replace-string`ed the regular halfwidth spaces to fullwidth spaces.

Happy Hanukkah!

Also, there is a funny story behind the CLISP logo you can read in <https://www.gnu.org/software/clisp/impnotes/faq.html#faq-menorah>.  In this section there is a link to an old (2001-05-15) "gnu.emacs.help" newsgroup thread linked from there.  It is a dead link, but you can read the whole thing on <https://groups.google.com/g/gnu.emacs.help/c/R7wEPCJXuaI/m/MbKL0zeQXioJ> instead.

Maybe a more amusing thing is that the authors are already aware of that dead link, and the new URL will replace the old dead one in the next release version, but the commit to have fixed this link is 8 years old, so one has to wonder - when will the next release come………? (<https://sourceforge.net/p/clisp/bugs/752/>)
