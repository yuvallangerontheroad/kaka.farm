title: Hanukkah Is Ruined Because Emojis Do Not Conform To Monospace Fonting.
date: 2024-12-25 02:30
tags: hanukiyah, hanukkah, hanukkahmenorah, israel, judaism, kaka, khanukah, khanukiyah, poo, poop
---

I have tried copying the clisp[1] khanukiyah[2], adding some poo on top, and then lighting it on fire for Hanukkah, but the emoji just would not align with the monospace font.

Khanukah is ruined.  Ruined!  You can all go home now.

The attempts:

```
🔥　🔥　🔥　🔥　🔥　🔥　🔥
💩　💩　💩　💩　💩　💩　💩
ＩＩ　ＩＩ　ＩＩ　ＩＩ　ＩＩ　ＩＩ　ＩＩ
ＩＩ　　＼　　｀－＋＋－＇　　／　　ＩＩ
　＼　　　｀－－－＋＋－－－＇　　　／
　　｀－＿＿＿＿＿｜｜＿＿＿＿＿－＇
　　　　　　　　　｜｜
－－－－－－－－－＋＋－－－－－－－－－

🔥 🔥 🔥 🔥 🔥 🔥 🔥
💩 💩 💩 💩 💩 💩 💩
II II II II II II II
II  \  `-++-'  /  II
 \   `---++---'   /
  `-_____||_____-'
         ||
---------++---------
```

[1]: https://www.gnu.org/software/clisp/
[2]: https://en.wikipedia.org/wiki/Hanukkah_menorah
