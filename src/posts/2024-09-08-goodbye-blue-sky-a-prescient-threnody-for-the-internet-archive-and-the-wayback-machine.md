title: Goodbye Blue Sky A Prescient Threnody For The Internet Archive And The Wayback Machine.
date: 2024-09-08 17:30
tags: bookburning, copyrights, digitalholocaust, freeculture, goodbyebluesky, pinkfloyd, threnody
---

The [Internet Archive][ia], a massive library of copyrighted and public domained digital and digitised priceless artefacts of major (or minor (or non-whatsoever)) historical significance, and the [Wayback Machine][wm], a vast archive of major (or minor (or non-whatsoever)) historical significance dead web pages, are apparently being killed off by book publishing houses.

They've lost major copyright legal battle and are headed towards financial ruin.  All of their work - for naught.

The first phrase and song appearing in conscious thought related to this book burning jamboree the book publishers are taking part of was [Goodbye Blue Sky][gbbs] by the Pink Floyd.

Details:

- [The Internet Archive Loses Appeal.  As Expected.][ll] (There's a [Youtube video][ly] where the article is being read if that's your thing).
- [Internet Archive Loses Landmark E-Book Lending Copyright appeal against publishers][tf].

[ia]: https://archive.org/
[wm]: https://web.archive.org/
[gbbs]: https://www.youtube.com/watch?v=rKBz5_pbdzM
[ll]: https://lunduke.locals.com/post/6079435/the-internet-archive-loses-appeal-as-expected
[ly]: https://www.youtube.com/watch?v=az6y2vxKmhc
[tf]: https://torrentfreak.com/internet-archive-loses-landmark-e-book-lending-copyright-appeal-against-publishers-240905/
