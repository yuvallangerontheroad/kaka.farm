title: Fairy Genocide
author: Yuval Langer
date: 2022-11-16 15:31
tags: fairies, genocide, magic, pixies, war
---

It had been a few months after the peace delegation had disappeared
without a trace deep in the Human territories south of the Forest. The
war with the Human was at a standstill and both parties had decided to
attempt peace negotiations. Now news of atrocities started to pour
in. Whole villages gone to the Human aggressor, with an odd survivor
reaching the Forest capital, telling strange stories of powerful Human
warlocks possessing spells capable of striking down all Fairies around
them, even stranger was that those warlocks were children.

--------

The Serrate Leafs' General, Baron Rhubarb "Rub" Popkin, was well
rested after a day and a half in the luxurious royal suite. The place
was immense, fitted for Human proportions. He got up from his bed, the
size of a Nutball field, and flew to the little drawers, normally used
to store paper and envelopes, on top of the enormous desk where he
stored his uniforms. He was far away from home, here in the Human
capital, many hundreds of kilometres to the south of The Forest.

"Today we will make history and stop this bloody bloody war," thought
Rub.

He met the rest of the delegation outside, all already mounted on top
of their rabbits. He mounted his rabbit, Lazylegs, and they marched to
a tent of gargantuan proportions where the ceremony was held,
signalling the beginning of the peace talks.

The ceremony started with music, loud and obnoxious, from loud and
obnoxious instruments the size of a building. There have been an
accompaning dance routine.

"Flailing hills, these Humans," the General Baron thought.

After the stage cleared he was invited to go to his seat at the
table. A wooden chair with fine filligries, custom made for this
occassion. It was badly proportioned and uncomfortable.

"A little back ache for the safety of our people."

The Human Myriads' Master, Monsieur Tepid Pool, had finally greeted
the parties and made a little speech - the horrors of the century long
war, the price both Human and Fairy had spent on this standstill
military engagement, the lost years and lost lives and what could have
been but will never be.

It was Popkin's turn. A Human relay was assigned to him, relaying in
his thunderous Human voice what Popkin had said in the regular
loudness of his own voice. He talked of Fairy culture - their affinity
to the woods, their cities in the treetops, their lives with the
forest animals and the Gnomes of the burrows, absent from the
delegation as they were still suspicious of Humans, the magic that
made them a match to giant Humans.

The relay repeated his words, of Fairy magic, and a small child in the
audience shouted loudly:

"Ay doon believe in magic!"

The Human child was promptly shushed, but the damage was done. Popkin
and his delegation suddenly screamed, zooming through the air,
scattering in all directions, and eventually slowly fell like autumn
leafs to the floor. Only their rabbit mounts were left of the
delegation. Lazyleg slowly hopped to his master's body, like the rest
of the rabbits hopping to theirs', and gently sniffed then nudged the
lifeless body of Baron Popkin.

From the stage, Tanisloff Gregorin - Top Commander of the Human
military - considered the child with a wide grin and knew that the war
was now far from over, but already won.
