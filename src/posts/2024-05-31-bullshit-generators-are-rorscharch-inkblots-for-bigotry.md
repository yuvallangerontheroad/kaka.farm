title: Bullshit Generators Are Rorschach Inkblots For Bigotry.
date: 2024-05-31 14:00
tags: bigotry, csj, freesoftware, misandry
---

Two things:

1. Bigotry is bad.
2. The worst part of this whole thing is the hypocrisy.

[@civodul@toot.aquilenet.fr] said [1]:

> “Is Stack Overflow Obsolete? An Empirical Study of the Characteristics of ChatGPT Answers to Stack Overflow Questions”
> https://arxiv.org/html/2308.02312v4
>
> More evidence that ChatGPT & co. are, indeed, mansplaining-as-a-service—in addition to being a resource drain.

[@ghisvail@framapiaf.org] said [2]:

> @civodul "Our analysis shows that 52% of ChatGPT answers contain incorrect information and 77% are verbose."
>
> Yep, sounds about like mansplaining.

[1]: https://toot.aquilenet.fr/@civodul/112534738418631897
[2]: https://framapiaf.org/@ghisvail/112534824202863387
[@civodul@toot.aquilenet.fr]: https://toot.aquilenet.fr/@civodul
[@ghisvail@framapiaf.org]: https://framapiaf.org/@ghisvail
