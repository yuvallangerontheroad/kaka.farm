title: Duck Typing Made Me Hate The Wikipedia.
date: 2024-09-21 15:34
tags: nofunallowed, prose, wikipedia
---

I hate the Wikipedia.

It sucks the fun out of every topic it touches.

For most of the time it was a situation of "[you didn't notice, but your brain did][harrysplinkett]".

I just didn't think about it.

Eventually the contrast between the sublime topics the Wikipedia documents and its dull prose I was reading became so apparent that it had surfaced upon my consciousness:

"The Wikipedia is so boring," the thought had spoken.

But I didn't hate.

I didn't hate the Wikipedia until I had a conversation.

The conversation was about duck typing in programming languages.

I was looking for a good whimsical metaphor for it, one I had previously read on the Wikipedia.

## The Pond, The Ducks, And The Fire Breathing Dragon

Ages ago, beyond the wirling mists of time, I've read a bit on the Wikipedia regarding duck typing.

I found it illuminating.

I found it joyful, whimsy.

It is no longer there.

Someone removed it.

It involved a closely guarded duck-filled pond, and a wily fire breathing dragon.

It went something like this:

> In essence, the problem is that, if it walks like a duck and quacks like a duck, it could be a dragon doing a duck impersonation.  One may not always want to let dragons into a pond, even if they can impersonate a duck.

Actually, it went exactly like this.  I looked up "dragon" on on that page with some external tool named [WikiBlame] and found it.

It was added on [this revision], at 2008-04-11T17:07:51 by an anonymous IP number 212.3.160.125, and was deleted on [that revision]†, at 2013-11-20T05:46:09Z by a pseudonymous [Aldaron], a so called "Wikipedian", with the message:

> Funny, but not illuminating.

It lived in Wikipedia for about five and a half years.

## "Funny, but not illuminating."

As you can see, we only have the IP number of the original contributer at the time of submission, and we will never know who wrote it.  It was given to us gratis, free from egotistic concern.

On the other hand, we do know who had removed it.

This "[Aldaron]" Wikipedian.  This grey automaton.

A ticktockman, sucking the joy out of every fucking Wikipedia article.

Think of a topic you hold dear.

Maybe it is birding?

Maybe you're into tornadoes?

Extremely fast cars?

The deepness of geologic time and the strange creatures we find in it?

The far away bodies we see in telescopes, the weird suns, dim distant galaxies, the vast stretches of void?

Maybe it's the hard problem of consciousness and the question "what exactly am I"?

Anything.

Now suck the joy out of it.  Describe it in the most bland language possible.  Throw away any whimsy.  Throw away awe.  Throw away metaphors.  No, awe and joy are NPOV.  Do we need 'em?  No?  Then chuck 'em.

When you look something up on the Wikipedia, you find a your awe diminished, dessicated, within prose so stale, so bland that you would rather be staring at a rock.  That is not that bad.  Geologists find them fascinating.

[Aldaron]: https://en.wikipedia.org/wiki/User:Aldaron
[WikiBlame]: https://wikipedia.ramselehof.de/wikiblame.php?user_lang=en&lang=en&project=wikipedia&tld=org&article=Duck+typing&needle=dragon&skipversions=0&ignorefirst=0&limit=500&offmon=9&offtag=21&offjahr=2024&searchmethod=int&order=desc&user=
[harrysplinkett]: https://www.youtube.com/watch?v=lsnGmyqYzvI
[that revision]: https://en.wikipedia.org/w/index.php?title=Duck_typing&oldid=582473883
[this revision]: https://en.wikipedia.org/w/index.php?title=Duck_typing&oldid=204920921
