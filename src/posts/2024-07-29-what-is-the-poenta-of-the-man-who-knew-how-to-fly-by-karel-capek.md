title: What Is The Poenta Of The Man Who Knew How To Fly By Karel Čapek?
date: 2024-07-29 10:40
tags: confusionwillbemyepitaph, fiction, karel-capek, poenta, shortstory, story
---
What is the poenta of [The Man Who Knew How To Fly by Karel Čapek][fly]?

Is it a metaphor against government intervention in a free market?

Is it a metaphor against government poking around the private lives of people?

Maybe it is a case against overconfident experts advising on things on which they are not experts?

The more abstract message of individuality over conformity?

What else?

[fly]: https://tzin.bgu.ac.il/~arikc/fly.html
