title: GNU Emacs Surrealism Mode Is A Writing Tool That Stops You From Editing Your Words.
date: 2024-08-04 18:30
tags: code, elisp, emacs, emacslisp, freesoftware, gnuemacs, lisp, minormode, software, streamofconsciousness, surrealism, writing, writingtool
---

Wrote a rudimentary stream-of-consciousness writing major mode in the spirit of #unix_surrealism and for those who wish a mechanical contraption that'll stop them from erasing and editing their words while in the writing flow state zone.

<https://codeberg.org/kakafarm/emacs-surrealism-mode>

If you think it could be improved, or have some more key chords I didn't think of (I didn't think of many chords, almost none), please send me a patch.
