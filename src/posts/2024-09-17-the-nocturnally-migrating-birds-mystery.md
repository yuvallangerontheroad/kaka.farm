title: The Nocturnally Migrating Birds Mystery
date: 2024-09-17 01:28
tags: audio, audiosurvey, avianmigration, biology, birds, migration, nightmigration, survey, zoology
---

Turns out there are birds that only do their seasonal migration during the dark hours of the night.

You might think: "You cannot even see birdies at night!  What are we gonna do now?!"

I'm not an expert, but while reading the Wikipedia I've come across a [sublime bit][wp] (roughly summarising and dropping the irrelevant):

> One method for night-time migration observation are audio recording surveys.
> A downside of this method is that some birds have different calls during the night than during the day.

So we're left wondering what do these birds even look like - there are a bunch of chirps on audio files and we might never know who's the chirper.

[wp]: https://he.wikipedia.org/w/index.php?title=%D7%A0%D7%93%D7%99%D7%93%D7%AA_%D7%A2%D7%95%D7%A4%D7%95%D7%AA&oldid=39584566#%D7%A9%D7%99%D7%98%D7%AA_%D7%A9%D7%9E%D7%A2
