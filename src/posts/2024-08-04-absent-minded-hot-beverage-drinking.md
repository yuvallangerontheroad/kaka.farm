title: Absent Minded Hot Beverage Drinking.
date: 2024-08-04 16:51
tags: existentialquandry, hevelhavalim, worldofdew
---

The worst thing about various hot beverages is drinking them absentmindedly, not really enjoying the thing, and then coming back to the cup and finding it already empty.  That's life.  Life in a teacup.

[Or a beer bottle, in the case of Going For A Bear by Robert Coover](https://www.youtube.com/watch?v=d0hgT0qj0qU).
