title: The First Video About CSJ by Thunderf00t?
date: 2024-06-10 01:47
tags: addiction, csj, thunderf00t, youtube
---

Dr. Phil E. Mason, known under the monicker "Thunderf00t", is a long
time (almost 20 years now?) science communicator on Youtube and an
academic chemist.  Along the way he has seen and commented on many of
the great mental malignancies of the Western world.  One of these
things was the flowering of the CSJ Blight.

I was wondering what was the first Thunderf00t video concerning this
Blight and it seems that it started, at least for Mason, with
Dr. P.Z. Myers and his ironically named Free Thought Blogs.  Myers was
very much like Mason, criticising the various ills of religion and
other [New Atheist][New Atheism] this-or-thats, but in text-form
rather than video-form, writing on his blog - Pharyngula.

Eventually some of these New Atheists decided to congregate within
their own blog hosting platform they called Free Thought Blogs.  At a
certain point Mason also joined, but after a very short stay they
kicked him out for having reasoned opinions, what else...

So here it is, published on 2012-07-05, the first video of the
Thunderf00t CSJ arc:

[PZ Myers and the Art of Shameless Dishonesty].

Now, looking back at the unfolding events since around that time and
culminating with the recent CSJ calls for genocide………  oh boy, what a
ride, and it isn't over yet……… ……… ………

You know, I have an advice for you:

> Don't get born in interesting times.

[New Atheism]: https://en.wikipedia.org/wiki/New_Atheism
[PZ Myers and the Art of Shameless Dishonesty]: https://www.youtube.com/watch?v=o1mLHdTsmPc
