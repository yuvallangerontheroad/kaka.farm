title: 'murica! - A Poem.
date: 2024-11-28 10:34
tags: advertisement, africa, ancient, blahblah, cocacola, crapsaccharine, old, poem, reuven, starvation, tagstagstags, yadayada
---

Sent 2011-12-29 18:19 Israeli time.

Email title was: 'murica!

body:

> A sub-Saharan Tanzanian boy is drinking coke.
> Bloated belly, empty eyes and matchstick arms and legs to match.
> Coca Cola, Pepsi, Dr. Pepper. Flies.
> Boy drinks, then smiles faintly: "Life begins here."
> At last, the flies and self digestion finally overcome and he plunges
> into oblivion while thinking of that distant American dream.
