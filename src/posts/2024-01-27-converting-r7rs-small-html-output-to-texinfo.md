title: Converting R7RS-small HTML output to Texinfo.
date: 2024-01-27 20:37
tags: lisp, r7rs, rnrs, scheme, texinfo
---

I am converting the [HTML output][r7rs-small-html] of
[R7RS-small][r7rs-small] into [Texinfo][texinfo].  It is currently on
<https://codeberg.org/kakafarm/guile/src/branch/work-in-progress-texinfo-r7rs-small/doc/r7rs-small/r7rs-small.texinfo>.
[Cargoculting][cargocult] from the previous [R5RS Texinfo source
file][r5rs] that lives in the neighbouring directory.

[r7rs-small-html]: https://standards.scheme.org/corrected-r7rs/r7rs.html
[r7rs-small]: https://small.r7rs.org/
[texinfo]: https://www.gnu.org/software/texinfo/
[cargocult]: https://en.wikipedia.org/wiki/Cargo_cult_programming
[r5rs]: https://codeberg.org/kakafarm/guile/src/branch/work-in-progress-texinfo-r7rs-small/doc/r5rs/r5rs.texi
