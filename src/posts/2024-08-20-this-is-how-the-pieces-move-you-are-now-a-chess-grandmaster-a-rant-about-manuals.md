title: This Is How The Pieces Move.  You Are Now A Chess Grandmaster!  (A Rant About Manuals)
tags: api, documentation, fsf, gnu, guile, learning, manuals, programming, rant, scheme
date: 2024-08-20 04:31
---

You want to learn how to use a library, right?  You open the manual, let's say the [Guile manual section on SRFI-171][srfi-171].  You read a bunch of impenetrable handwavy prose.  You read an API reference for a bunch of procedure definitions, describing their input arguments, their behaviour and output.

Now, what do you think will happen as you finish reading "EOF object" in the last procedure definition of the "Helper functions for writing transducers" page?

## First option:

As you finish reading the SRFI-171 chapter, golden heavenly light envelopes your head as your brain forms complete cosmic understanding of transducers and their place within the fabric of reality. Congratulations, you are now The Boddhisatva of GC-Friendly Collection-Agnostic Data-Processing.  You dedicate your life to converting code generating many intermediate lists unecessarily into one that uses transducers, thereby reducing GC churn.

## Second option:

You are confused more than before reading the manual.  The manual has about 3 trivial examples showing how to use the API, teaching you almost nothing about how to put the pieces together into a coherent whole nor how these pieces should be used with other bits and pieces of code outside the SRFI-171.  You go to GNU Emacs and start writing a ranty post.

## Solution?

One thing I would improve is adding many more examples, in the same way that [The Little Schemer][littleschemer] (and the rest of books by Daniel P. Friedman of this kind) builds tacit understanding of Scheme (and other topics) by providing many examples of increasing complexity.

Also, how about hiring a technical writer for your manuals, eh?

[srfi-171]: https://www.gnu.org/software/guile/manual/html_node/SRFI_002d171.html
[littleschemer]: https://mitpress.mit.edu/9780262560993/the-little-schemer/
