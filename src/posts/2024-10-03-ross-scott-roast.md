title: Ross Scott Roast.
date: 2024-10-03 10:58
tags: accursedfarms, fiction, forums, pleasedontsueme, roast, rossscott
---

[Ross Scott][accursedfarms] makes funny videos on the Internet.  He has a talent for looking at fantastic things with a down to earth very mundane attitude.

A totalitarian conquering space alien force from another dimension?  Why, let's talk about "do you know who ate all the jelly doughnuts?" kind of [office politics][friday] and the environmental impact of losing more than [4/5 of the population][oilswell].

What about a [desert racing game][trackmania2canyon] filled with fancy tracks, you know, the loop de loop kind?  Talk about bored billionaires building fancy tracks for life sized autonomic bots, and the possibility of a global catastrophe causing total human extinction, of course. (huh, human extinction comes up quite a lot in his catalogue)

He is also the champion behind our best and only hope for the future of [computer game preservation][stopkillinggames].

Now, there's a [Ross Scott Roast][rossscottroast] thread over at his site's forums and I the following, inspired by a [certain video][superculttycoon] of his:

---

Me and Ross go way back.  We've known each other for ages now, and I'll tell you - the man has no integrity.

Back then we had a few business ventures together, all legit, of course, until he comes up with the bright idea of running motivational speeches.

You see, I have a knack for public speaking and he's an exceptionally good writer, so I'm the face of the operation while he's working in the background, writing them speeches, right?

We do exactly that, and we start attracting a bunch of die hard fans.  Some people just have to get their motivational pep talk, you see.

After a while he decides we should start a co-housing group, with some steep admission fees, of course.  Ya gotta run the place somehow. 😉

It'll be for all them like minded individuals who want to live with similarly like minded individuals - those with too much money and not enough sense.

Some time passes, and we're all getting comfortable with the arrangement.  Fancy house, fancy food and wine, surrounded by sycophants willing to do whatever we tell 'em.  That's nice.

But good things are not meant to be forever.

The Man starts sniffing around, and they have no good reason to do that, we're not breaking any law, we're just living our lives peacefully, I swear.

We go to a fancy restaurant, Mediterranean I think, and I whisper to Ross about the ominous visit.  He just nods and tells me he'll sort it out.  I tell him I have a bad feeling about it.

Next day a whole troop comes in and breaks the place down.  As I am carried off outside the gates of our village, guess what I see?

It's Ross getting a pouch full of money from an officer of the law.

He ratted me out faster than you can say "Intentional Community"!

And for what?  For thirty stinking pieces of silver!

[accursedfarms]: https://www.accursedfarms.com/
[friday]: https://www.accursedfarms.com/posts/civil-protection/friday/
[oilswell]: https://www.accursedfarms.com/posts/civil-protection/oils-well/
[rossscottroast]: https://www.accursedfarms.com/forums/topic/6068-say-something-bad-about-ross-or-his-stupid-show-here/page/2/#comment-255761
[stopkillinggames]: https://www.stopkillinggames.com/
[superculttycoon]: https://www.accursedfarms.com/posts/rosss-game-dungeon/super-cult-tycoon/
[trackmania2canyon]: https://www.accursedfarms.com/posts/rosss-game-dungeon/rgdtrackcanyon/
