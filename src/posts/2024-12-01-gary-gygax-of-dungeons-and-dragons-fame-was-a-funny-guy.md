title: Gary Gygax of Dungeons And Dragons Fame Was A Funny Guy.
date: 2024-12-01 04:57
tags: campy, dnd, dungeonsanddragons, garygygax, gygax, humour, osr, rpg, ttrpg
---

A [comment on Youtube][1] by username [@cowpercoles1194][2] wrote about a funny bit Gary Gygax did in his own game guidebooks:

> Back in the 70s, Gygax was criticized for including tons of unnecessary tables in the D&D rules.  When he wrote the Urban Random Encounter table in the DMG, he added a "Harlot subclass table" so the DM could roll up exactly what kind of Harlot the party randomly encountered in a city.  Then he flipped through a thesaurus to find a bunch of flowery adjectives as descriptions.  The whole thing was a joke --it was a ridiculous 'easter egg' hidden in the back of the DMG, with the hopes that when a DM rolled an encounter and flipped to the page, they were presented with a ridiculous table complete with completely unnecessary procedures to follow.  The only thing more ridiculous than this table, is actually taking it seriously.

This is great, innit?

[1]: https://www.youtube.com/watch?v=vduGDQOLd6A&lc=Ugwrk7hS4JZZ3bhDwnl4AaABAg
[2]: https://www.youtube.com/@cowpercoles1194
