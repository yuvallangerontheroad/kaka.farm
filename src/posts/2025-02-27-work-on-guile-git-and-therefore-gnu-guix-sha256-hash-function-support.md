title: Work On guile-git And Therefore GNU Guix SHA256 Hash Function Support.
date: 2025-02-27 03:56
tags: bot, c, cc, clang, guile, guix, irc, ircbot, libgit2, programming, tips
---

(Amateurtip: If you are confused by `pkg-config`'s inability to provide you with flags for a certain library, remember to use `pkg-config --list-all` to list all available package names.)

Recently you had the bright idea of using the [guile-irc][guile-irc] library for writing an [IRC bot][ircbot].

The library did not work.

What do you do?

You go in and copy its source, try to make sense of it, adapting it for your use as you trace its logic.

At a certain point you see how messages coming from the server are parsed, [using regular expressions][regexp], and you do not like it.

It seems much nicer to use a nice looking [PEG][wikipediapeg] defined in [clearly named symbolic expressions][peg].

What do you do?

You attempt to rewrite the parsing of these messages using the [PEG library][peg] library provided with [GNU Guile][guile].

But another problem appears - the [RFC defines the syntax of these messages in terms of bytes][rfc], but the GNU Guile PEG library only deals with strings.

What do you do?

You take [the GNU Guile PEG library][ice-9 peg] and [convert it to use bytevectors instead of strings][bv-peg].

Now that most of the work is done, you want to package it in a [GNU Guix][guix] package.

Problem is - the `guile-bytevector-peg` repository is an SHA256 hash function object based git repository and GNU Guix does not yet support this kind of git repository.

What do you do?

You start working on getting [SHA256 hash function based git repository support][git-sha256-transition] into [guile-git][guile-git], which is the GNU Guile git library used by GNU Guix.

Here you hit the basalt rock of your rabbit hole spelunking.

`libgit2` only has experimental support for SHA256 git repositories and would only declare stable SHA256 git repository support [in the next major release][libgit2-v1.9.0-release-notes], that is - `libgit2 v2.0.0`.

You still try building and using the latest version of `libgit2`, that is `v1.9.0`.

You [killyank][killyank] the [libgit2-1.8][libgit2-1.8] package definition from the GNU Guix repository, tweak it a bit, and place it in your [personal Guix Channel][guixpackage].

This result in at least two pains in your arse:

- Turns out that if you [give cmake the `"-DEXPERIMENTAL_SHA256=ON` flag][experimentalflag] when building `libgit2`, the `pkg-config` package name would be `libgit2-experimental` and you would need to use `#include <git2-experimental.h>` in your C source code.

    (Now that I think of it, it makes sense when developing on old-timey GNU distributions which do not have `guix shell` with which to build separate container environments and in which to develop and test your libraries...)
- Some files in libgit2 have `#include <git2/...>` instead of `#include <git2-experimental/...>`.  It is noteworthy because there **is** no `git2/` directory in the headers directory, just `git2-experimental/`!

Okay, this post is too long, you think.

What do you do?

You end it abruptly.

[bv-peg]: https://codeberg.org/kakafarm/guile-bytevector-peg/
[experimentalflag]: https://codeberg.org/kakafarm/guix-kakafarm-channel/src/commit/626a019ea263e012c923fd2643558c1e0dee09ed/kakafarm/packages/version-control.scm#L59
[git-sha256-transition]: https://git-scm.com/docs/hash-function-transition
[guile-git]: https://gitlab.com/guile-git/guile-git/-/issues/32
[guile-irc]: https://github.com/rekado/guile-irc
[guile]: https://www.gnu.org/software/guile/
[guix]: https://guix.gnu.org/
[guixpackage]: https://codeberg.org/kakafarm/guix-kakafarm-channel/src/commit/626a019ea263e012c923fd2643558c1e0dee09ed/kakafarm/packages/version-control.scm#L33
[ice-9 peg]: https://git.savannah.gnu.org/cgit/guile.git/tree/module/ice-9/peg.scm
[ircbot]: https://en.wikipedia.org/wiki/IRC_bot
[killyank]: https://www.emacswiki.org/emacs/KillingAndYanking
[libgit2-1.8]: https://git.savannah.gnu.org/cgit/guix.git/tree/gnu/packages/version-control.scm?id=8a3e8f36e2a90e51fa4559beeaed1a86585d4b25#n1375
[libgit2-v1.9.0-release-notes]: https://github.com/libgit2/libgit2/releases/tag/v1.9.0
[peg]: https://www.gnu.org/software/guile/manual/html_node/PEG-Parsing.html
[regexp]: https://github.com/rekado/guile-irc/blob/7d08ce6fdcb87ac668c5d3bfd5584247805507bb/irc/message.scm#L138
[rfc]: https://www.rfc-editor.org/rfc/rfc2812.html#section-2.3.1
[wikipediapeg]: https://en.wikipedia.org/wiki/Parsing_expression_grammar
