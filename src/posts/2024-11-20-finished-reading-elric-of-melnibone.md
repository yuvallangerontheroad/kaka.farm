title: Finished Reading Elric of Melnibone.
date: 2024-11-20 08:58
tags: books, elric, fantasy, michaelmoorcock, moorcock, stormbringer, theeternalchampion
---

Finished reading Elric of Melniboné by Michael Moorcock.  It was **so** fun.

The Melnibonians, of which our protagonist, Elric, is the emperor, are an ancient race of psychopathic fey-like men living on an Island - The Dragon Isle.

Their culture widely uses slavery and rape and torture and drugs (for slaves and masters) and dark eldritch magic.

But there is a twist!  Elric is not a psychopath like the rest.  He is therefore criticised for choosing not to employ many of the psychotically sadistic Melnibonian traditions in daily matters and in matters of military strategy, like a true Melnibonian, like one of his forefathers.

And the status and power of Melniboné is waning, it had been for centuries, and if it continues they might be utterly destroyed.

All of this makes for an interesting read.

Now to the second installment - The Sailor on the Seas of Fate.

Oh, and here is an amusing bit of fun - their head torturer, the thin tall man in charge of extracting secrets through bleeding flesh, is called Doctor Jest!  How fun is that?  Very!
