title: Writing A Fancy URL Choosing Menu For The Mouse Deficient And The Emacs Addicted.
date: 2024-11-14 6:54
tags: cheeky, emacs, gnu, gnuemacs, internet, keyboard, mouse, pointer
---

I am working on a [GNU Emacs package][emacs-ffap-urls-menu] which will allow one to scan the current buffer for URLs and filenames, and then open a [Tabulated List mode][tabulated-list-mode] (actually a mode [derived][derived-modes] from that) buffer in which the user may mark which URLs to open.

This Tabulated List mode is used by [`list-buffers`][list-buffers] and [`list-packages`][list-packages], allowing them to create an interactive table in which the user may choose which entries to delete, which to install, which to open, or any other action defined in each respective package.

What I find joyful is that once upon a time, before a [particular piece][dictionarycom] of [Internet vernacular][wiktionary] could [blossom][googletrends], the package which deals with scanning for filepaths and URLs was named ["Find File At Point"][ffap], and the command which (re-)builds the list of URLs is named `ffap-menu-rescan`.  Considering what [the Internet is for][avenueq], the ancients who had named this package were prescient.

I still have no idea what I am doing, so I am going through the `Buffer-menu` package from `buff-menu.el` and renaming it all from `buffers-menu-*` to `ffap-urls-menu-*`, not automatically, but manually, just so I can read the code top to bottom.  I am doing so with a wee smile on my face, a glint in my eyes, and a skip in my fingers.

(Honestly, fellas, why did you think I've named this place "Kaka Farm"?)

[avenueq]: https://www.youtube.com/watch?v=zi8VTeDHjcM
[derived-modes]: https://www.gnu.org/software/emacs/manual/html_node/elisp/Derived-Modes.html
[dictionarycom]: https://www.dictionary.com/e/slang/fap/#origin
[emacs-ffap-urls-menu]: https://codeberg.org/kakafarm/emacs-ffap-urls-menu
[ffap]: https://www.gnu.org/software/emacs/manual/html_node/emacs/FFAP.html
[googletrends]: https://trends.google.com/trends/explore?date=2004-01-01%202024-11-14&q=fap
[list-buffers]: https://www.gnu.org/software/emacs/manual/html_node/emacs/List-Buffers.html
[list-packages]: https://www.gnu.org/software/emacs/manual/html_node/emacs/Packages.html
[tabulated-list-mode]: https://www.gnu.org/software/emacs/manual/html_node/elisp/Tabulated-List-Mode.html
[wiktionary]: https://en.wiktionary.org/wiki/fap#Etymology_2
