title: The Original Undiluted MIT Jargon File
date: 2024-01-16 08:11
tags: scheme, mit, lisp, unix, culture, history
---

Turns out there's an original undiluted MIT-centric Lisp-supremacist
UNIX-culture-bashing version of the Jargon File:

<https://www.dourish.com/goodies/jargon.html>

(from <https://mumble.net/> (from
<https://mumble.net/~campbell/scheme/foof-loop.txt> (a guide for better
Scheme loopings)))
