title: Youtube Channel RSS Feed.
date: 2025-01-02 22:56
tags: feed, rss, youtube
---

(copied more or less verbatim from <https://codeberg.org/kakafarm/guile-youtube-rss/src/branch/master/README.org>, (after a `org-md-export-as-markdown` in GNU Emacs)).

I have written GNU Guile script that outputs the RSS feed of a Youtube channel for each video URL of that channel.  You can find it on <https://codeberg.org/kakafarm/guile-youtube-rss/>.

## Why?

If you want to follow Youtube channels using your chosen feed reader, you can:

1.  Get a channel ID, like `UC0BvmQ-UWDnDc9seBcSanRA`.
2.  Append it at the end of <https://www.youtube.com/feeds/videos.xml?channel_id=>.  You get <https://www.youtube.com/feeds/videos.xml?channel_id=UC0BvmQ-UWDnDc9seBcSanRA>.
3.  Add it to a feed reader of your choosing (which is probably <https://github.com/skeeto/elfeed>).

What if you do not have the channel ID?

What if you only have the uploader ID, the one prefixed with the strudel sign (`@`), like in the case of <https://www.youtube.com/@SexuaLobster/>?

You cannot use it as <https://www.youtube.com/feeds/videos.xml?channel_id=@SexuaLobster>.  It gives you a 404 error.

To the best of my knowledge, you cannot use any other variable name instead of `channel_id`.

I have also tried `uploader_id`, `username`, `user_name`, and `user_id`, but they all return a 404 error.

What you need is to copy the address to one of the videos of that channel and provide it as input to this wee Guile script.

## Usage.

### Single URL.

If you have a video URL you would want to convert, you can run:

```sh
printf "https://www.youtube.com/watch?v=YqDRA60AgNs" | guile youtube-rss.scm
```

It will output the following:

```
https://www.youtube.com/feeds/videos.xml?channel_id=UC0BvmQ-UWDnDc9seBcSanRA
```

### A file filled with URLs.

If you have a bunch of them in a file called `newline-separated-youtube-video-urls.list` (yes, they must be newline separated):

```
https://www.youtube.com/watch?v=LJ1gInUG5uw
https://www.youtube.com/watch?v=sdwtMtaIkGM
```

Run the command:

```sh
guile youtube-rss.scm \
    < newline-separated-youtube-video-urls.list \
    >> youtube-channel-rss-feeds.list
```

You now have a file called `youtube-channel-rss-feeds.list` filled with RSS feed URLs:

```
https://www.youtube.com/feeds/videos.xml?channel_id=UCZ8Ql34vQn91fzq0xvG9Pqg
https://www.youtube.com/feeds/videos.xml?channel_id=UCkK9UDm_ZNrq_rIXCz3xCGA
```

### On a Guix shell container.

If you want to run it in a [Guix shell container](https://guix.gnu.org/manual/en/html_node/Invoking-guix-shell.html), you would run:

```sh
guix shell \
    --container \
    --network \
    curl \
    guile \
    guile-srfi-180 \
    nss-certs \
    yt-dlp \
    -- guile youtube-rss.scm \
    < newline-separated-youtube-video-urls.list \
    >> youtube-channel-rss-feeds.list
```

## Dependencies.

In a Guix installation, these are the packages on which the script depends:

-   curl.
-   guile-srfi-180.
-   guile.
-   nss-certs.
-   yt-dlp.
