date: 2024-01-17 07:36
title: The Gemini protocol is a Dadaist multimedia art installation impotently raging against the enshittification of the World Wide Web.
tags: dadaism, enshittification, gemini, www
---

A steaming hot take from the bottom of the Ceramic Throne:

The Gemini protocol is a Dadaist art installation.  And as the
Dadaists impotently raged against what they had perceived to be an
unjust war a hundred or so years ago[1], so do the new Dadaists, the
Gemini developers and users impotently rage against the
enshittification of the World Wide Web.  The reason it exists is
because it feels nice to be doing something, even when it is
empirically ineffective.

Also, I keep misspelling it as "Dataism" due to muscle memory, but
this mistake seems to be a rather good name for Gemini and the Small
Web art movements………

[1]: https://www.youtube.com/watch?v=H_ltPg4ysuE
