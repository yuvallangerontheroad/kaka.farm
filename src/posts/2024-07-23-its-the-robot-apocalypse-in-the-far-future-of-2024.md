title: It's The Robot Apocalypse In The Far Future of 2025.
date: 2024-07-23 11:06
tags: fiction, microfic, science-fiction, scifi, story
---

It's the robot apocalypse in the far future of 2025.

A small team of human survivors, covered in ash and tatters, are
huddling in the shadows inside a building.  One of them peeks outside
the window, seeing a lone military robot walking in the street.

One of them, a young man: "We're out of ammo, we're out of batteries
for our EM emitters, and food is nearly completely gone.  What are we
gonna do?!"

The bunch is silent for half a minute and another of them, a
bespectacled wizardly man with a fractured lens starts smiling, a
glint in his eyes, and says:

"You know, back in the olden days"; The young man interjects: "17th
century?".  "No, 2024", the old man answers, continues: "I worked as
an IT engineer and..."

He jumps to his feet and starts moving towards the door, outside, and
continues fractically:

"And we had a trick against the machines!"

He runs towards the roving robot, gaining its attention.  The robot
faces the man and there's a moment of silence.  The man starts:

```
User-agent: *
Disallow: /
```

The story is more likely than you'd think:

<https://www.nytimes.com/2024/07/19/technology/ai-data-restrictions.html>

Thanks Demp for that one bit.
