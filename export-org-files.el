(require 'org)

(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (emacs-lisp . t)
   (scheme     . t)
   (shell      . t)
   ))

(defun export-org-to-markdown (filename)
    (let* ((basename (file-name-sans-extension filename))
           (markdown-filename (concat basename ".md")))
      (find-file filename)
      ;; (org-babel-execute-buffer t)
      (let* ((title (car (org-property-values "post_title")))
             (date (car (org-property-values "post_date")))
             (tags (car (org-property-values "post_tags"))))
        (find-file (org-md-export-to-markdown))
        (goto-char (point-min))
        (insert (format "title: %s
date: %s
tags: %s
---

"
                        title
                        date
                        tags))
        (save-buffer))))

(let ((org-confirm-babel-evaluate nil)
      (org-export-with-toc nil))
  (pcase command-line-args-left
    ('()
     (error "Got no filenames!"))
    (filenames
     (dolist (filename filenames)
       (message "org file: %s" filename)
       (export-org-to-markdown filename)))))
