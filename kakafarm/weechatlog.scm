(define-library (kakafarm weechatlog)
  (import (scheme base)
          (srfi :9)
          (srfi :19)
          (ice-9 match)
          (ice-9 peg))
  (export parse-weechatlog
          weechatlog-line?
          weechatlog-datetime
          weechatlog-user-status
          weechatlog-nickname
          weechatlog-message)

  (begin
    (define-record-type <weechatlog-line>
      (make-weechatlog-line datetime
                            user-status
                            nickname
                            message)
      weechatlog-line?
      (datetime weechatlog-datetime)
      (user-status weechatlog-user-status)
      (nickname weechatlog-nickname)
      (message weechatlog-message))

    (define-peg-string-patterns
      "weechatlog <-- (NL* weechatlog-line)* NL*
weechatlog-line <-- datetime-field T BRA user-status nickname-field KET T message-field
datetime-field <-- (!NL !T .)*
nickname-field <-- (!NL !T !KET .)*
message-field <-- (!NL .)*
user-status <-- (.)
BRA < '<'
KET < '>'
T < '\t'
NL < '\n'")


    (define (parse-datetime str)
      (string->date str "~Y-~m-~dT~H:~M:~S~z"))

    (define (parse-weechatlog str)
      (let ((tree (peg:tree (match-pattern weechatlog str))))
        (match tree
          (`(weechatlog
             . ,weechatlog-lines)
           (map parse-weechatlog-line
                weechatlog-lines))
          (else
           (error "Bad weechatlog:" tree)))))

    (define (parse-weechatlog-line tree)
      (match tree
        (`(weechatlog-line (datetime-field ,datetime-field)
                           (user-status ,user-status)
                           (nickname-field ,nickname-field)
                           (message-field ,message-field))
         (make-weechatlog-line (parse-datetime datetime-field)
                               user-status
                               nickname-field
                               message-field))))))
