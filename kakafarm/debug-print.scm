(define-library (kakafarm debug-print)
  (export
   dp
   pp
   )
  (import
   (scheme base)
   (scheme write)

   (srfi srfi-28)

   (only (guile) current-source-location)

   (only (ice-9 pretty-print) pretty-print)
   )
  (begin
    (define-syntax dp
      (syntax-rules ()
        ((dp x)
         (let* ((location (current-source-location))
                (filename (cdr (assq 'filename location)))
                (line (cdr (assq 'line location)))
                (column (cdr (assq 'column location)))
                (x-res x))
           (display "DEBUG: ")
           (display filename)
           (display ":")
           (display line)
           (display ":")
           (display column)
           (newline)
           (display "(quoted):")
           (newline)
           (pretty-print (quote x))
           (display "(unquoted):")
           (newline)
           (pretty-print x-res)
           x-res))))

    (define (pp attribute record)
      (pretty-print (list attribute
                          (attribute record))))
    ))
