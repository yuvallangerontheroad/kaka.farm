(define-library (kakafarm links)
  (import
   (scheme base)

   (only (guile)
         assq-ref
         define*
         )

   (kakafarm debug-print)
   )
  (export make-sxml)

  (begin
    (define links
      '(((url . "https://analognowhere.com/")
         (url-text . "techno-mage in unix_surrealism!")
         (desc . "Dreamlike storytelling mirroring our contemporary free software versus proprietary software wars."))
        ((url . "https://systemcrafters.net/")
         (url-text . "David Wilson's Systemcrafters!")
         (desc . "The guy mostly talks and demonstrates GNU Emacs, Guix, and GNU Guile stuff, but not just that."))
        ((url . "https://accursedfarms.com/")
         (url-text . "Ross Scott's Accursed Farms!")
         (desc . "Obscure computer games reviews and Freeman's Mind series."))
        ((url . "https://sigwinch.xyz/")
         (url-text . "SIGWINCH")
         (desc . "Zipheir (also known as Wolfgang Corcoran-Mathe (sorry, I'll never know
your \"real\" name because I use your nickname so much)) is basically
a Scheme and Lisp encyclopedia.  He writes SRFIs for FUN!"))
        ((url . "http://e-neko.co/misc/book/")
         (url-text . "Shores of the EndlessSea")
         (desc . "A science fiction novel a good friend of mine is writing.  Seems to be
on hiatus.  I hope he will continue writing in the future."))
        ))

    (define list-items
      (map (lambda (link)
             `(li (a (@ (href ,(assq-ref link 'url)))
                     ,(assq-ref link 'url-text))
                  " - "
                  ,(assq-ref link 'desc)))
           links))

    (define sxml
      `(div (ul ,@list-items)))

    (define* (make-sxml #:key posts)
      sxml)))
