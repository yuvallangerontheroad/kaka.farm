(define-library (kakafarm twtxt)
  (import (scheme base))
  (export twtxt-timeline)

  (begin
    (define twtxt-timeline
      (list (cons "2024-01-24 07:23:19+00:00" "Hello.")))))
