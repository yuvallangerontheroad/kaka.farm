(define-library (kakafarm site)
  (export
   haunt-footer
   creative-commons-copyright-notice
   header
   make-favicon-asset
   page-builder
   theme
   title-prefix
   ceramic-throne-img
   )
  (import
   (scheme base)

   (only (guile) define*)

   (prefix (haunt artifact) haunt:artifact:)
   (prefix (haunt asset) haunt:asset:)
   (prefix (haunt html) haunt:html:)
   (prefix (haunt builder blog) haunt:builder:blog:)
   (prefix (haunt site) haunt:site:)
   )
  (begin
    (define ceramic-throne-img
      '(img (@ (src "/images/logo.png")
               (alt "A superflat vector drawing of a ceramic throne."))))

    (define creative-commons-copyright-notice
      '(p (@ (xmlns:cc "http://creativecommons.org/ns#")
             (xmlns:dct "http://purl.org/dc/terms/"))
          (a (@ (property "dct:title")
                (rel "cc:attributionURL")
                (href "https://kaka.farm/")) "Kaka Farm")
          " by "
          (a (@ (rel "cc:attributionURL dct:creator")
                (property "cc:attributionName")
                (href "https://kaka.farm/")) "Yuval Langer")
          " is licensed under "
          (a (@ (href
                 "http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1")
                (target "_blank")
                (rel "license noopener noreferrer")
                (style "display:inline-block;"))
             " Attribution-ShareAlike 4.0 International "
             (img (@ (style
                         "height:22px!important;margin-left:3px;vertical-align:text-bottom;")
                     (src
                      "https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1")
                     (alt "")))
             (img (@ (style
                         "height:22px!important;margin-left:3px;vertical-align:text-bottom;")
                     (src
                      "https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1")
                     (alt "")))
             (img (@ (style
                         "height:22px!important;margin-left:3px;vertical-align:text-bottom;")
                     (src
                      "https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1")
                     (alt ""))))))

    (define haunt-footer
      '(footer (hr)
               (div (@ (id "webrings"))
                    "Webrings!"
                    (div (@ (class "craftering"))
                         (a (@ (href
                                "https://craftering.systemcrafters.net/@cow_2001/previous"))
                            ←)
                         (a (@ (href "https://craftering.systemcrafters.net/"))
                            craftering)
                         (a (@ (href
                                "https://craftering.systemcrafters.net/@cow_2001/next"))
                            →)))
               (hr)
               (p "Built with "
                  (a (@ (href "https://dthompson.us/projects/haunt.html"))
                     "David Thompson's Haunt")
                  ", a "
                  (a (@ (href "https://www.gnu.org/software/guile/")) "GNU Guile")
                  " Static Site Generator.  Source code for the site can be found in my "
                  (a (@ (href "https://kaka.farm/~stagit/kaka.farm/log.html"))
                     "Stagit")
                  ", "
                  (a (@ (href "https://codeberg.org/kakafarm/kaka.farm")) "Codeberg")
                  ", or "
                  (a (@ (href "https://git.sr.ht/~kakafarm/kaka.farm/"))
                     (s "Source Hut"))
                  "† ("
                  (a (@ (href "/posts/drew-devault-and-source-hut.html"))
                     "why I stopped using sr.ht")
                  ").  Find more sites written with Haunt at "
                  (a (@ (href "https://awesome.haunt.page/")) "Awesome Haunt")
                  "!")
               (hr)
               (p "Support me 💰 on "
                  (a (@ (href "https://buymeacoffee.com/kakafarm/"))
                     "https://buymeacoffee.com/kakafarm/")
                  "!")
               (hr)
               (p "Mirror on "
                  (a (@ (href "https://kakafarm.codeberg.page/"))
                     "Codeberg Page")
                  ". (This site, "
                  (a (@ (href "https://kaka.farm/"))
                     "https://kaka.farm/")
                  ", is not searchable on any search engine, except for (for some strange reason) "
                  (a (@ (href "https://yandex.com/search?text=site%3Akaka.farm"))
                     "on Putin's search engine")
                  ", and a "
                  (a (@ (href "https://search.seznam.cz/?q=kaka+farm"))
                     "Czech search engine called Seznam")
                  ", so I also mirror it on "
                  (a (@ (href "https://codeberg.page/"))
                     "Codeberg")
                  ", otherwise, it is on "
                  (a (@ (href "https://kaka.farm/"))
                     "https://kaka.farm/")
                  ".)")
               (hr)))

    (define header
      `(h1 (a (@ (href "/"))
              ,ceramic-throne-img
              (br)
              "Kaka Farm!")))

    (define (make-favicon-asset)
      (haunt:asset:make-asset "assets/logo.png" "/favicon.ico"))

    (define* (make-page a-site an-sxml-tree title #:key meta-description-content)
      (let ((meta-description (if meta-description-content `((meta (@ (name "description") (content ,meta-description-content)))) '())))
        `((doctype "html")
          (html (@ (lang "en"))
                (head (meta (@ (charset "UTF-8")))
                      (link (@ (rel "stylesheet")
                               (type "text/css")
                               (href "/static/styles.css")))
                      (title ,title)
                      ;; The people of #systemcrafters (acdw and daviwil) say it's a good thing for mobile shenanigans.
                      ;;
                      ;; Also see https://stackoverflow.com/questions/68626280/understanding-the-viewport-meta-tag linked by acdw.
                      (meta (@ (name "viewport")
                               (content "width=device-width,initial-scale=1")))
                      (meta (@ (name "author")
                               (content "Yuval Langer")))
                      ,@meta-description
                      (link (@ (rel "alternate")
                               (type "application/atom+xml")
                               (title "Kaka Farm Blog Feed!")
                               (href "/feed.xml"))))
                (body (div (@ (id "topmostdiv"))
                           ,header
                           ,an-sxml-tree
                           ,haunt-footer
                           ,creative-commons-copyright-notice))))))

    (define* (page-builder #:key destination make-sxml theme title meta-description-content)
      (let ((title (if title
                       (string-append title-prefix " - " title)
                       title-prefix))
            (layout (haunt:builder:blog:theme-layout (theme #:meta-description-content meta-description-content))))
        (lambda (site posts)
          (haunt:artifact:serialized-artifact destination
                                              (layout site title (make-sxml #:posts posts))
                                              haunt:html:sxml->html))))

    (define* (theme #:key meta-description-content)
      (haunt:builder:blog:theme #:name "topsite-theme"
                                #:layout
                                (lambda (a-site a-page-title-string an-sxml-tree)
                                  (make-page a-site
                                             an-sxml-tree
                                             a-page-title-string
                                             #:meta-description-content meta-description-content))))

    (define title-prefix
      "💩 Kaka Farm! 💩")
    ))
