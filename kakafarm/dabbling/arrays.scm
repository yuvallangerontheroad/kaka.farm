(define-module (kakafarm dabbling arrays)
  #:export (
            array-ref
            array-set!
            array-shape
            make-typed-array

            array-vector
            array-shape-multiples
            )
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9))

(define-record-type <array>
  (%make-array vector shape shape-multiples)
  array?
  (vector array-vector)
  (shape array-shape)
  (shape-multiples array-shape-multiples))

(define (make-shape-multiples shape)
  (fold-right (lambda (x acc) (cons (* x (car acc)) acc))
              '(1)
              (cdr shape)))

(define* (make-typed-array type fill #:rest shape)
  (%make-array (make-vector (apply * shape) fill)
               shape
               (make-shape-multiples shape)))

(define* (array-ref array #:rest indices)
  (vector-ref (array-vector array)
              (array-indices->vector-index indices
                                           (array-shape-multiples array))))

(define* (array-set! array value #:rest indices)
  (let* ((vector (array-vector array))
         (shape-multiples (array-shape-multiples array))
         (vector-index (array-indices->vector-index indices shape-multiples)))
    (vector-set! vector
                 vector-index
                 value)))

(define (array-indices->vector-index indices shape-multiples)
  (let* ((mult (map (lambda (index shape-multiple)
                 (* index shape-multiple))
               indices
               shape-multiples))
         (sum (apply + mult)))
    sum))
