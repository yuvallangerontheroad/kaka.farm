(define-module (kakafarm dabbling arrays-tests)
  #:use-module (srfi srfi-64)

  #:use-module ((kakafarm dabbling arrays)
                #:prefix kf::)
  )

(test-begin "ARRAYS-TESTS")

(let ((number-of-rows 2)
      (number-of-columns 3))
  (test-equal (list->vector (iota (* number-of-rows number-of-columns)))
    (let ((arr (kf::make-typed-array 'u8 0 number-of-rows number-of-columns)))
      (kf::array-shape-multiples arr)
      (do ((row-index 0 (1+ row-index)))
          ((>= row-index number-of-rows))
        (do ((column-index 0 (1+ column-index)))
            ((>= column-index number-of-columns))
          (let ((value (+ column-index
                          (* row-index
                             number-of-columns))))
            (kf::array-set! arr
                            value
                            row-index
                            column-index))))
      (kf::array-vector arr))))

(test-end "ARRAYS-TESTS")
