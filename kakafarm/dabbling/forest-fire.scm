(define-library (kakafarm dabbling forest-fire)
  (export
   display-forest-fire
   forest-fire-step!
   make-forest-fire
   moore-neighbour-is-lit?
   von-neumann-neighbour-is-lit?
   )

  (import
   (scheme base)
   (scheme write)

   (only (guile)
         1+
         define*
         )

   (ice-9 match)

   (kakafarm dabbling arrays)
   )

  (cond-expand
   (guile (import (srfi srfi-27)))
   (hoot (import (hoot ffi)))
   )

  (begin
    (cond-expand
     (hoot (define-foreign random-real "math" "random" -> f64))
     (else))

    (define-record-type <forest-fire>
      (%make-forest-fire array-current
                         array-next
                         ignition-probability
                         fill-probability
                         neighbour-is-lit?
                         )
      forest-fire?
      (array-current forest-fire-array-current set-forest-fire-array-current!)
      (array-next forest-fire-array-next set-forest-fire-array-next!)
      (ignition-probability forest-fire-ignition-probability)
      (fill-probability forest-fire-fill-probability)
      (neighbour-is-lit? forest-fire-neighbour-is-lit?)
      )

    (define (swap-forest-fire-arrays! forest-fire)
      (let ((array-current (forest-fire-array-current forest-fire))
            (array-next (forest-fire-array-next forest-fire)))
        (set-forest-fire-array-current! forest-fire array-next)
        (set-forest-fire-array-next! forest-fire array-current)))

    (define* (make-forest-fire #:key
                               number-of-rows
                               number-of-columns
                               ignition-probability
                               fill-probability
                               (neighbour-is-lit? moore-neighbour-is-lit?))
      (let ((forest-fire (%make-forest-fire (make-typed-array 'u8 0 number-of-rows number-of-columns)
                                            (make-typed-array 'u8 0 number-of-rows number-of-columns)
                                            ignition-probability
                                            fill-probability
                                            neighbour-is-lit?)))
        forest-fire))

    (define (forest-fire-number-of-rows forest-fire)
      (let ((shape (array-shape (forest-fire-array-current forest-fire))))

        (car shape)))

    (define (forest-fire-number-of-columns forest-fire)
      (cadr (array-shape (forest-fire-array-current forest-fire))))

    (define bare-value 0)
    (define tree-value 1)
    (define fire-value 2)
    (define (bare? cell) (= cell bare-value))
    (define (tree? cell) (= cell tree-value))
    (define (fire? cell) (= cell fire-value))

    (define (random-event? probability)
      (< (random-real) probability))

    (define (forest-fire-neighbour-from-offset forest-fire row-index column-index row-offset column-offset)
      (array-ref (forest-fire-array-current forest-fire)
                 (modulo (+ row-index row-offset)
                         (forest-fire-number-of-rows forest-fire))
                 (modulo (+ column-index column-offset)
                         (forest-fire-number-of-columns forest-fire))))

    (define (moore-neighbour-is-lit? forest-fire row-index column-index)
      (or
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index -1 -1))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index -1  0))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index -1  1))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index  0 -1))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index  0  1))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index  1 -1))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index  1  0))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index  1  1))))

    (define (von-neumann-neighbour-is-lit? forest-fire row-index column-index)
      (or
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index -1  0))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index  0 -1))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index  0  1))
       (fire? (forest-fire-neighbour-from-offset forest-fire row-index column-index  1  0))))

    (define (forest-fire-step! forest-fire)
      (let ((current-array (forest-fire-array-current forest-fire))
            (next-array (forest-fire-array-next forest-fire)))
        (do ((row-index 0 (1+ row-index)))
            ((>= row-index (forest-fire-number-of-rows forest-fire)))
          (do ((column-index 0 (1+ column-index)))
              ((>= column-index (forest-fire-number-of-columns forest-fire)))
            (let ((current-cell (array-ref current-array row-index column-index)))
              (match current-cell
                ((? bare?)
                 (if (random-event? (forest-fire-fill-probability forest-fire))
                     (array-set! next-array tree-value row-index column-index)
                     (array-set! next-array bare-value row-index column-index)))
                ((? tree?)
                 (cond
                  ((random-event? (forest-fire-ignition-probability forest-fire))
                   (array-set! next-array fire-value row-index column-index))
                  (else
                   (if ((forest-fire-neighbour-is-lit? forest-fire) forest-fire row-index column-index)
                       (array-set! next-array fire-value row-index column-index)
                       (array-set! next-array tree-value row-index column-index)))))
                ((? fire?)
                 (array-set! next-array bare-value row-index column-index)))))))
      (swap-forest-fire-arrays! forest-fire))

    (define (display-forest-fire forest-fire)
      (do ((row-index 0 (1+ row-index)))
          ((>= row-index (forest-fire-number-of-rows forest-fire)))
        (do ((column-index 0 (1+ column-index)))
            ((>= column-index (forest-fire-number-of-columns forest-fire)))
          (match (array-ref (forest-fire-array-current forest-fire)
                            row-index
                            column-index)
            ((? bare?) (display "."))
            ((? tree?) (display "#"))
            ((? fire?) (display "^"))
            (unknown
             (error "Unknown tile:" unknown)))
          )
        (newline)))
    ))
