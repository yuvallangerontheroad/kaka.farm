(define-library (kakafarm contact)
  (export make-sxml)
  (import (guile))

  (begin

;;; XXX: This attempt at factoring out lists from HTML did not go as planned……… ……… ………
    
    (define irc-address-list
      '("ircs://irc.libera.net/systemcrafters"
        "ircs://irc.libera.net/unix_surrealism"
        "ircs://irc.quakenet.org/israel"
        "ircs://irc.rizon.net/israel"))

    (define (irc-address->sxml-list-item irc-address)
      `(li (a (@ (href ,irc-address))
              ,irc-address)))
    
    (define sphere-address-list
      `(
        ("Activitypub"
         ("Now on "
          (a (@ (href "https://shitposter.world/kakafarm/")) "@kakafarm@shitposter.world")
          "!"
          (br)
          "(previously on: "
          (s (a (@ (href "https://emacs.ch/@kakafarm")) "@kakafarm@emacs.ch"))
          " (RIP " (a (@ (href "https://emacs.ch/"))
                     "https://emacs.ch†.")
          "))"))
        ((a (@ (href "https://en.wikipedia.org/wiki/Internet_Relay_Chat")) "IRC")
         ("Nickname cow_2001 on:"
          (ul ,(map irc-address->sxml-list-item
                    irc-address-list))))
        ))
    
    (define* (make-sxml #:key posts)
      `((h1 "Contact:")
        (dl
         ,(map (lambda (sphere-address)
                 `((dt ,(car sphere-address))
                   (dd ,(cdr sphere-address))))
               sphere-address-list))))))
