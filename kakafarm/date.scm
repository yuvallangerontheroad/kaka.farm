(define-library (kakafarm date)
  (export
   format-date-as-iso-8601-utc
   )
  (import
   (scheme base)

   (srfi srfi-19))
  (begin
    (define (format-date-as-iso-8601-utc date)
      (let* ((time-utc (date->time-utc date))
             (date-at-utc (time-utc->date time-utc 0)))
        (date->string date-at-utc "~4")))
    ))
