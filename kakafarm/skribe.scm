(define-module (kakafarm skribe)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 textual-ports))

(define-public (sh-runner code)
  (with-input-from-port (open-pipe code OPEN_READ)
    (lambda ()
      (get-string-all (current-input-port)))))

(define-public (show-code-and-result code-runner code)
  (let ((result (code-runner code)))
    `(p (pre ,code)
        "result: " ,result)))

