(define-library (kakafarm software clipboard-speaker)
  (import (guile))
  (export make-sxml)

  (begin
    (define* (make-sxml #:key posts)
      `(div
        (h2 "GNU Guile Clipboard Speaker")

        (p "An accessibility tool that reads the marked text or the clipboard contents.")
        (p "In X there are three different clipboards, so you need to decide from
which of those three clipboards you'd get the text.")
        (p "Normally you would set four of the window manager's key bindings for
this command - one for each of the three clipboards (I myself only set
bindings for two of the clipboards), and the fourth for killing the
program.")

        (dl
         (dt "Read the marked text:")
         (dd (pre "clipboard-speaker --clipboard-type=p"))

         (dt "Read the regular clipboard contents:")
         (dd (pre "clipboard-speaker --clipboard-type=b"))

         (dt "Stop all that racket:")
         (dd (pre "clipboard-speaker --kill"))

         (dt "Code repository:")
         (dd (a (@ (href "https://codeberg.org/kakafarm/guile-clipboard-speaker/"))
                "Clipboard Speaker's Codeberg repository")))

        (h3 "A helpful help message:")

        (pre
         "$ clipboard-speaker --help
Usage: clipboard-speaker [--kill]
                         [-f]
                         [-l]
                         [-w 230]
                         [--clipboard-speaker-path=]
                         [--clipboard-type=b/s/p (choose b or s or p)]
                         [--words-per-minute=170]

Keywords:
  --clipboard-speaker-path      Path to the clipboard-speaker settings directory.
  --clipboard-type              Type of clipboard (b, s, or p)
  --fifo-file-path          -f  Path to FIFO file.
  --kill                        Kill the espeak-ng loop process.
  --lock-file-path          -l  Path to lock file.
  --words-per-minute            Words per minute spoken.
  --words-per-minute        -w  Words per minute spoken.")

        (h2 "Python Clipboard Speaker")

        (p (em "NOTICE: ")
           "The Python version is obsolete.  Use the GNU Guile one instead.")

        (dl
         (dt "Read the currently marked text:")
         (dd (pre "clipboard-speaker -p"))

         (dt "Read the clipboard contents:")
         (dd (pre "clipboard-speaker -b"))

         (dt "The original Python code:")
         (dd (a (@ (href "https://codeberg.org/kakafarm/clipboard-speaker/"))
                "Python Clipboard Speaker's Codeberg repository")))))))
