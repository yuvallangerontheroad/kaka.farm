(define-library (kakafarm software diceware)
  (import (guile))
  (export make-sxml)

  (begin
    (define* (make-sxml #:key posts)
      `(div
        (h2 "Rusty Diceware")
        (p
         (a (@ (href "https://codeberg.org/kakafarm/rusty-diceware/"))
            "A diceware tool in Rustlang."))

        (pre "$ diceware --help
Usage: diceware [options]

Options:
    -h, --help          This help message.
    -e, --entropy       Display number of entropy bits.
    -r, --dicerolls     Provide results of physical dice rolls. Word per line,
                        same digit order as in the files, digits between and
                        including 1 and 6.
    -n, --nword NWORD   Number of words in a passphrase.
    -d, --delimiter DELIM
                        The delimiter character used to separate the words.
    -f, --wordlist-file FILE
                        Path to a wordlist file.
    -l, --wordlist WORDLIST
                        Wordlist to use. (efflong (default), effshort1,
                        effshort2, minilock, reinhold, or beale)")))))
