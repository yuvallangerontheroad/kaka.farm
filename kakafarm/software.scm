(define-library (kakafarm software)
  (import
   (guile)

   (ice-9 match)
   )
  (export make-sxml)

  (begin
    (define emacs-stuff
      (list
       (list
        "https://codeberg.org/kakafarm/super-duper-yes-or-no"
        "Emacs Super Duper Yes or No"
        "Alternatives to yes-or-no-p whose affirmative is even more of an hassle to type in than \"yes\"")
       (list
        "https://codeberg.org/kakafarm/emacs-nano-tts-minor-mode/"
        "Emacs Nano TTS Minor Mode"
        "A minor mode which reads whatever is currently marked")
       (list
        "https://codeberg.org/kakafarm/emacs-wordlists/"
        "Emacs Wordlists"
        "Emacs Wordlists is a package which packages lists of words for such things as passphrase generators, or, generally, when one wants to encode a number in a list of words")
       (list
        "https://codeberg.org/kakafarm/emacs-opml-to-elfeed-feeds"
        "Emacs OPML To Elfeed Feeds"
        "Download and convert OPML (Outline Processor Markup Language) files to the Elfeed elfeed-feeds list")
       (list
        "https://codeberg.org/kakafarm/emacs-surrealism-mode"
        "Emacs Surrealism Mode"
        (list
         "A minor mode for writing without editing.  Enables stream of consciousness type of writing exercise.  Inspired by pmjv / prahou of "
         '(a (@ (href "https://analognowhere.com/"))
             "techno-mage in unix_surrealism")))
       (list
        "https://codeberg.org/kakafarm/fancy-urls-menu"
        "Fancy URLs Menu (previously known as Fancy FFAP Menu)"
        (list
         "Interface for viewing and opening URLs in current buffer.  Available on "
         '(a (@ (href "https://melpa.org/#/fancy-urls-menu"))
             "MELPA - "
             (img (@ (alt "MELPA")
                     (src "https://melpa.org/packages/fancy-urls-menu-badge.svg")))))))
      )

    (define emacs-thing->li
      (match-lambda
        ((address address-text description)
         `(li (a (@ (href ,address)) ,address-text) " - " ,description "."))))

    (define* (make-sxml #:key posts)
      `(div
        (div "Some stuff I wrote:"
             (ul (li (a (@ (href "diceware.html"))
                        "Diceware!"))
                 (li (a (@ (href "clipboard-speaker.html"))
                        "Clipboard Speaker!"))
                 (li
                  (a (@ (href "https://codeberg.org/kakafarm/guix-kakafarm-channel/"))
                     "GNU Guix Kakafarm Channel!")
                  " (a modest GNU Guix channel I manage).  Also here on my "
                  (a (href "/~stagit/guix-kakafarm-channel/log.html")
                     "Stagit")
                  ".")))
        (div "A few GNU Emacs packages:"
             (ul ,@(map emacs-thing->li emacs-stuff)))))))
