(define-library (kakafarm blog)
  (export
   theme
   title-prefix
   )
  (import
   (scheme base)

   (prefix (srfi srfi-19) srfi-19:)

   (prefix (haunt post) haunt:post:)
   (prefix (haunt site) haunt:site:)
   (prefix (haunt builder blog) haunt:builder:blog:)

   (only (kakafarm debug-print) dp)
   (prefix (kakafarm date) kakafarm:date:)
   (prefix (kakafarm site) kakafarm:site:)
   )
  (begin
    (define title-prefix "💩 Kaka Farm Blog! 💩")

    (define (make-post-page a-site an-sxml-tree a-post-title)
      (let ((head-title (string-append title-prefix " - " a-post-title)))
        `((doctype "html")
          (html (@ (lang "en"))
                (head (meta (@ (charset "UTF-8")))
                      (link (@ (rel "stylesheet")
                               (type "text/css")
                               (href "/static/styles.css")))
                      (title ,head-title)
                      ;; The people of #systemcrafters (acdw and daviwil) say it's a good thing for mobile shenanigans.
                      ;;
                      ;; Also see https://stackoverflow.com/questions/68626280/understanding-the-viewport-meta-tag linked by acdw.
                      (meta (@ (name "viewport")
                               (content "width=device-width,initial-scale=1")))
                      (meta (@ (name "author")
                               (content "Yuval Langer")))
                      (link (@ (rel "alternate")
                               (type "application/atom+xml")
                               (title "Kaka Farm Blog Feed!")
                               (href "/feed.xml"))))
                (body (div (@ (id "topmostdiv"))
                           (h1 (a (@ (href "/"))
                                  ,kakafarm:site:ceramic-throne-img
                                  (br)
                                  "Kaka Farm!")
                               (br)
                               (a (@ (href "/posts/"))
                                  "The Kaka Farm Blog!"))
                           (h2 ,a-post-title)
                           ,an-sxml-tree
                           ,kakafarm:site:haunt-footer
                           ,kakafarm:site:creative-commons-copyright-notice))))))

    (define (make-post-url url-prefix site post)
      "Return a POST's full URL built from a URL-PREFIX string, a SITE object, and a POST object."
      (let* ((date (haunt:post:post-date post))
             (year (srfi-19:date->string date "~Y"))
             (month (srfi-19:date->string date "~m"))
             (day (srfi-19:date->string date "~d"))
             (url
              (string-append url-prefix
                             "/"
                             (haunt:site:site-post-slug site post)
                             ".html")))
        url))

    (define (post->sxml-link post)
      `(a (@ (href ,(string-append (haunt:post:post-slug-v2 post) ".html")))
          ,(srfi-19:date->string (srfi-19:time-utc->date (srfi-19:date->time-utc (haunt:post:post-date post)) 0) "~4")
          " - "
          ,(cdr (assq 'title
                      (haunt:post:post-metadata post)))))

    (define theme
      (haunt:builder:blog:theme
       #:name "blog-theme"
       #:layout
       (lambda (a-site a-page-title-string an-sxml-tree)
         (make-post-page (string-append (haunt:site:site-title a-site) " - " title-prefix)
                         an-sxml-tree
                         a-page-title-string))
       #:post-template
       (lambda (a-post)
         (let ((date-iso-6801 (srfi-19:date->string (haunt:post:post-date a-post) "~4")))
           `(("Date published: "
              (time (@ (datetime ,date-iso-6801))
                    ,date-iso-6801))
             ,(haunt:post:post-sxml a-post)
             "Tag feeds:"
             (ul ,@(map (lambda (tag)
                          `(li (a (@ (href ,(string-append "/feeds/"
                                                           tag
                                                           ".xml")))
                                  ,tag)))
                        (haunt:post:post-tags a-post))))))
       #:collection-template
       (lambda (a-site a-title-string a-list-of-posts a-url-prefix-string)
         `(ul ,(map (lambda (a-post)
                      (let* ((post-url (make-post-url a-url-prefix-string a-site a-post))
                             (metadata (haunt:post:post-metadata
                                        a-post))
                             (title (cdr (assq 'title
                                               metadata)))
                             (date (cdr (assq 'date
                                              metadata)))
                             (date-string (kakafarm:date:format-date-as-iso-8601-utc
                                           date)))
                        `(li (a (@ (href ,post-url))
                                ,date-string " - "
                                ,title))))
                    a-list-of-posts)))
       #:pagination-template
       (lambda (a-site an-sxml-tree
                       the-file-name-of-the-previous-page
                       the-file-name-of-the-next-page)
         `((div (a (@ (href ,the-file-name-of-the-previous-page))
                   "previous page")
                (a (@ (href ,the-file-name-of-the-next-page))
                   "next page"))
           (div ,an-sxml-tree)))))
    ))
