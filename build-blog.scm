(define-module (build-blog))

(import
 (prefix (srfi srfi-19) srfi-19:)

 (prefix (guile) guile:)

 (prefix (haunt builder assets) haunt:builder:assets:)
 (prefix (haunt builder atom) haunt:builder:atom:)
 (prefix (haunt builder blog) haunt:builder:blog:)
 (prefix (haunt post) haunt:post:)
 (prefix (haunt reader commonmark) haunt:reader:commonmark:)
 (prefix (haunt reader skribe) haunt:reader:skribe:)
 (prefix (haunt reader) haunt:reader:)
 (prefix (haunt site) haunt:site:)

 (prefix (kakafarm blog) kakafarm:blog:)
 (prefix (kakafarm contact) kakafarm:contact:)
 (prefix (kakafarm index) kakafarm:index:)
 (prefix (kakafarm links) kakafarm:links:)
 (prefix (kakafarm site) kakafarm:site:)
 (prefix (kakafarm software clipboard-speaker) kakafarm:software:clipboard-speaker:)
 (prefix (kakafarm software diceware) kakafarm:software:diceware:)
 (prefix (kakafarm software) kakafarm:software:)
 )

;; Moving blog from /haunt/posts/ to /posts/.
;;
;; '(define* (make-haunt-blog-index-sxml #:key site posts)
;;    (let ((five-last-posts (take (posts/reverse-chronological posts)
;;                                 (min (length posts) 5))))
;;      `(div (h2 "Kakafarm's Haunt")
;;            (ul (li (a (@ (href "/haunt/posts/")) "posts"))
;;                (li (a (@ (href "/haunt/feed.xml")) "atom feed"))))))
;;
;; (define* (haunt-blog-index-page-builder #:key destination make-sxml theme)
;;   (lambda (site posts)
;;     (serialized-artifact destination
;;                          ((theme-layout theme)
;;                           "" site-title-prefix
;;                           (make-sxml #:posts posts)) sxml->html)))

(define (file-filter filename)
  (and (haunt:site:default-file-filter filename)
       (not (guile:string-suffix? ".org" filename))))

(define (make-blog-post-slug post)
  "Return a POST's full URL built from a URL-PREFIX string, a SITE object, and a POST object."
  (let* ((date (haunt:post:post-date post))
         (year (srfi-19:date->string date "~Y"))
         (month (srfi-19:date->string date "~m"))
         (day (srfi-19:date->string date "~d"))
         (url
          (string-append year
                         "/"
                         month
                         "/"
                         day
                         "/"
                         (haunt:post:post-slug-v2 post)
                         )))
    url))

(define blog-site
  (haunt:site:site
   #:title kakafarm:blog:title-prefix
   #:build-directory "build/blog-site/"
   #:posts-directory "src/posts"
   #:domain "kaka.farm"
   #:make-slug make-blog-post-slug
   #:file-filter file-filter
   #:default-metadata
   '((author . "Yuval Langer")
     (email . "yuval.langer@gmail.com"))
   #:readers
   (list haunt:reader:commonmark:commonmark-reader
         haunt:reader:skribe:skribe-reader
         haunt:reader:sxml-reader)
   #:builders
   (list
    (haunt:builder:blog:blog
     #:theme kakafarm:blog:theme
     #:prefix "/posts"
     #:collections `(("Recent Posts"
                      "/posts/index.html"
                      ,haunt:post:posts/reverse-chronological)))
    ;; (page-builder #:destination "/haunt/index.html"
    ;;               #:make-sxml make-haunt-blog-index-sxml
    ;;               #:theme blog-theme)
    (haunt:builder:atom:atom-feed
     #:file-name "feed.xml"
     #:blog-prefix "/posts")
    (haunt:builder:atom:atom-feeds-by-tag
     #:prefix "feeds"
     #:blog-prefix "/posts"))))

(haunt:site:build-site blog-site)
