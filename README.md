[Kaka Farm](https://kaka.farm/)'s source code.

I forget, so I write. (Previously "Commonly Forgotten")

## Why is it split into `build-pages.scm` and `build-blog.scm`?

The two builds have different titles, which I could not do with a single build.

## Repositories, mirrors.

A mirror of the site can be found on:
https://kakafarm.codeberg.page/

Main repository:

<https://codeberg.org/kakafarm/kaka.farm/>

Mirror repositories:

- <https://kaka.farm/~stagit/kaka.farm/log.html>
