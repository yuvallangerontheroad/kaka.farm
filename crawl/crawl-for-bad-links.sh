#!/bin/bash

wget \
    --level=1 \
    --no-directories \
    --no-parent \
    --no-verbose \
    --output-file=kaka.farm.crawl.log \
    --recursive \
    --span-hosts \
    --spider \
    https://kaka.farm/
