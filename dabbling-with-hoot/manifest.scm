(use-modules
 (gnu packages base)
 (gnu packages guile)
 (gnu packages guile-xyz)

 (guix packages)
 )

(packages->manifest
 (list
  gnu-make
  guile-hoot
  guile-next
  ))
