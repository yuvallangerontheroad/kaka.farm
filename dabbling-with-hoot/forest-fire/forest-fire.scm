(import
 (scheme base)

 (only (hoot ffi)
       define-foreign
       procedure->external
       )

 (only (kakafarm dabbling forest-fire)
       display-forest-fire
       forest-fire-step!
       make-forest-fire
       von-neumann-neighbour-is-lit?
       )
 )

(define-foreign log "console" "log" (ref string) -> none)
(define-foreign random-real "math" "random" -> f64)
(define-foreign get-element-by-id "document" "getElementById" (ref string) -> (ref null extern))
(define-foreign set-element-text-content! "element" "setTextContent" (ref extern) (ref string) -> none)
(define-foreign timeout "window" "setTimeout" (ref extern) f64 -> none)

(define (make-demo)
  (make-forest-fire
   #:number-of-rows 20
   #:number-of-columns 80
   #:ignition-probability 0.001
   #:fill-probability 0.01
   #:neighbour-is-lit? von-neumann-neighbour-is-lit?))

(define forest-fire (make-demo))

(define dt 100.0)

(define (forest-fire-as-text forest-fire)
  (parameterize ((current-output-port (open-output-string)))
    (display-forest-fire forest-fire)
    (get-output-string (current-output-port))))

(define (log-forest-fire forest-fire)
  (log (forest-fire-as-text forest-fire)))

(define pre-element (get-element-by-id "pre"))

(define (set-pre-to-forest-fire forest-fire)
  (set-element-text-content! pre-element
                             (forest-fire-as-text forest-fire)))


(set-pre-to-forest-fire forest-fire)

(define (update)
  (set-pre-to-forest-fire forest-fire)
  (forest-fire-step! forest-fire)
  (timeout update-callback dt))
(define update-callback (procedure->external update))

(timeout update-callback dt)
