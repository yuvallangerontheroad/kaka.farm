window.addEventListener("load", async () => {
    try {
        await Scheme.load_main("forest-fire.wasm", {
            user_imports: {
                console: {
                    debug: (s) => console.debug(s),
                    log: (s) => console.log(s),
                    warn: (s) => console.warn(s),
                },
                window: {
                    setTimeout: (f, delay) => window.setTimeout(f, delay),
                },
                document: {
                    getElementById: (id) => document.getElementById(id),
                },
                element: {
                    setTextContent: (elem, text) => elem.textContent = text,
                },
                math: {
                    random: () => Math.random(),
                }
            }
        });
    } catch(e) {
        if(e instanceof WebAssembly.CompileError) {
            document.getElementById("wasm-error").hidden = false;
        }
        throw e;
    }
});
