posts = src/posts/*.md src/posts/*.sxml src/posts/*.skr # src/posts/*.org
kakafarm_source = $(shell find kakafarm -iname '*.scm')
# dabbling_source = $(shell find dabbling -iname '*.html' -o -name '*.js' -o -iname '*.css')

# poop:
#	for i in $(kakafarm_source) $(dabbling_source); do echo "$$i"; done

.PHONY: all
all: site

.PHONY: guile-force-auto-compile
guile-force-auto-compile: $(kakafarm_source)
	for i in $(kakafarm_source) ; do echo "$$i"; guile -L . -s "$$i" ; done

site: build/blog-site build/pages-site dabbling-with-hoot
	mkdir -p site/
	cp -rv build/pages-site/* build/blog-site/* site/
	rsync -rvP build/dabbling-with-hoot/ site/dabbling/

.PHONY: serve
serve: site
	python3 -m http.server --directory site/ && while sleep 0.1; do ls posts/* | entr -d make site; done

build/pages-site: $(kakafarm_source)
	LC_ALL=en_IL.utf8 guile -L . build-pages.scm

build/blog-site: $(posts) $(kakafarm_source)
	LC_ALL=en_IL.utf8 guile -L . build-blog.scm

.PHONY: upload-kaka.farm
upload-kaka.farm: site
	rsync -rP site/ haunt-at-kakafarm:/var/www/kaka.farm

.PHONY: upload-codeberg-pages
upload-codeberg-pages:
	rm -rvf ../pages/* && \
		rsync -rvvP site/ ../pages && \
		cd ../pages/ && \
		git add * && \
		git commit -m 'Update site.' && \
		git push pages pages

.PHONY: tests
tests: ARRAYS-TESTS.log

ARRAYS-TESTS.log: kakafarm/dabbling/arrays.scm
	guile -L . kakafarm/dabbling/arrays-tests.scm

.PHONY: clean
clean:
	rm -rvf build/* site/*

.PHONY: ares-server
ares-server:
	guix shell \
		guile-ares-rs \
		guile-hoot \
		guile-next \
		-- \
		guile -c '((@ (ares server) run-nrepl-server))'

### start dabbling.

forest_fire_files = \
	dabbling-with-hoot/forest-fire/*.js \
	dabbling-with-hoot/forest-fire/index.html

.PHONY: forest-fire-statics
forest-fire-statics: $(forest_fire_files)
	mkdir -p build/dabbling-with-hoot/forest-fire/
	guix shell bash guile-hoot -- bash -c 'cp -fv "$${GUIX_ENVIRONMENT}"/share/guile-hoot/*/{reflect-js/reflect.js,reflect-wasm/*.wasm} build/dabbling-with-hoot/forest-fire/'
	cp $(forest_fire_files) build/dabbling-with-hoot/forest-fire/

.PHONY: dabbling-with-hoot
dabbling-with-hoot: forest-fire-statics build/dabbling-with-hoot/forest-fire/forest-fire.wasm

build/dabbling-with-hoot/forest-fire/forest-fire.wasm: dabbling-with-hoot/forest-fire/forest-fire.scm
	mkdir -p build/dabbling-with-hoot/forest-fire/
	guix shell \
		-m dabbling-with-hoot/manifest.scm \
		-- guild compile-wasm \
			-L . \
			-o $@ $<

.PHONY: serve-dabbling
serve-dabbling-with-hoot: dabbling-with-hoot
	cd build/dabbling-with-hoot/
	pwd
	guix shell -m dabbling-with-hoot/manifest.scm -- guile -c '((@ (hoot web-server) serve))'

dabbling-clean:
	rm -vrf build/dabbling-with-hoot

### end dabbling.
