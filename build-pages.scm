(define-module (build-pages))

(import
 (prefix (guile) guile:)

 (prefix (haunt builder assets) haunt:builder:assets:)
 (prefix (haunt builder atom) haunt:builder:atom:)
 (prefix (haunt builder blog) haunt:builder:blog:)
 (prefix (haunt post) haunt:post:)
 (prefix (haunt reader commonmark) haunt:reader:commonmark:)
 (prefix (haunt reader skribe) haunt:reader:skribe:)
 (prefix (haunt reader) haunt:reader:)
 (prefix (haunt site) haunt:site:)

 (prefix (kakafarm blog) kakafarm:blog:)
 (prefix (kakafarm contact) kakafarm:contact:)
 (prefix (kakafarm index) kakafarm:index:)
 (prefix (kakafarm links) kakafarm:links:)
 (prefix (kakafarm site) kakafarm:site:)
 (prefix (kakafarm software clipboard-speaker) kakafarm:software:clipboard-speaker:)
 (prefix (kakafarm software diceware) kakafarm:software:diceware:)
 (prefix (kakafarm software) kakafarm:software:)
 )

;; Moving blog from /haunt/posts/ to /posts/.
;;
;; '(define* (make-haunt-blog-index-sxml #:key site posts)
;;    (let ((five-last-posts (take (posts/reverse-chronological posts)
;;                                 (min (length posts) 5))))
;;      `(div (h2 "Kakafarm's Haunt")
;;            (ul (li (a (@ (href "/haunt/posts/")) "posts"))
;;                (li (a (@ (href "/haunt/feed.xml")) "atom feed"))))))
;;
;; (define* (haunt-blog-index-page-builder #:key destination make-sxml theme)
;;   (lambda (site posts)
;;     (serialized-artifact destination
;;                          ((theme-layout theme)
;;                           "" site-title-prefix
;;                           (make-sxml #:posts posts)) sxml->html)))

(define (file-filter filename)
  (and (haunt:site:default-file-filter filename)
       (not (guile:string-suffix? ".org" filename))))

(define pages-site
  (haunt:site:site
   #:title kakafarm:site:title-prefix
   #:domain "kaka.farm"
   #:build-directory "build/pages-site/"
   #:file-filter file-filter
   #:default-metadata
   '((author . "Yuval Langer")
     (email . "yuval.langer@gmail.com"))
   #:readers
   (list
    haunt:reader:commonmark:commonmark-reader
    haunt:reader:skribe:skribe-reader
    haunt:reader:sxml-reader
    )
   #:builders
   (list
    ;; (page-builder #:destination "/haunt/index.html"
    ;;               #:make-sxml make-haunt-blog-index-sxml
    ;;               #:theme blog-theme)
    (kakafarm:site:page-builder
     #:destination "/index.html"
     #:make-sxml kakafarm:index:make-index-sxml
     #:theme kakafarm:site:theme
     #:meta-description-content "Kaka Farm - a poopy programmer's personal page!")
    (kakafarm:site:page-builder
     #:destination "/contact.html"
     #:make-sxml kakafarm:contact:make-sxml
     #:title "contact!"
     #:theme kakafarm:site:theme
     #:meta-description-content "Kaka Farm contact information.")
    (kakafarm:site:page-builder
     #:destination "/software/index.html"
     #:make-sxml kakafarm:software:make-sxml
     #:theme kakafarm:site:theme
     #:meta-description-content "Bits of software I have written.  Rarely updated.")
    (kakafarm:site:page-builder
     #:destination "/software/diceware.html"
     #:make-sxml kakafarm:software:diceware:make-sxml
     #:theme kakafarm:site:theme
     #:meta-description-content "A passphrase generators in Rustlang.")
    (kakafarm:site:page-builder
     #:destination "/software/clipboard-speaker.html"
     #:make-sxml kakafarm:software:clipboard-speaker:make-sxml
     #:theme kakafarm:site:theme
     #:meta-description-content "Accessibility tool that reads the contents of the clipboard or a marked bit of text.")
    (kakafarm:site:page-builder
     #:destination "/links/index.html"
     #:make-sxml kakafarm:links:make-sxml
     #:theme kakafarm:site:theme
     #:meta-description-content "Links to other nice things that are not part of Kaka Farm.")
    (haunt:builder:assets:static-directory "src/static" "static")
    (haunt:builder:assets:static-directory "src/images" "images")
    (haunt:builder:assets:static-directory "src/dabbling/html" "dabbling"))))

(haunt:site:build-site pages-site)
